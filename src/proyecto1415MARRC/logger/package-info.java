/**
 * <p>
 * Gestiona el fichero de log de la aplicación.
 * </p>
 *
 *
 * @author marrc1415
 * @since 1.0-FINAL
 * @version 1.0-FINAL
 */
package proyecto1415MARRC.logger;