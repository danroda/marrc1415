/*
 * Desarrollo de una pequeña herramienta que permita representar y razonar
 * (realizando inferencias) sobre una Red Bayesiana de una forma sencilla.
 * Métodos avanzados de razonamiento y representación del conocimiento, Máster
 * en Ingeniería Informática, curso 2014-2015, Universidad de Valladolid.
 * Autores: Borja Cerezo, Cristian Tejedor y Daniel Roda.
 */
package proyecto1415MARRC.logger;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Clase que gestiona él fichero Log de la aplicación.
 *
 * @author marrc1415
 * @since 0.2-BETA
 * @version 1.0-FINAL
 */
public final class Log {

    /**
     * Almacena el texto que irá al Log
     */
    private final static StringBuilder logTexto = new StringBuilder();

    /**
     * Almacena el estado del log. Guarda actividad si cierto, no hace nada si
     * falso.
     */
    private static boolean activo = true;

    /*
     * Crea un nuevo fichero si no existía.
     * Añade al fichero datos al final del mismo.
     */
    private Log() {
    }

    /**
     * Añade texto al log.
     *
     * @param nuevoTexto Texto que se añade al log
     */
    public static void append(final String nuevoTexto) {
        if (activo) {
            logTexto.append(nuevoTexto);
        }
    }

    /**
     * Guarda en el fichero de Log el texto acumulado.
     */
    public static void volcado() {
        if (activo) {
            try {
                // true segundo parámetro para añadir al fichero
                final PrintWriter impresor = new PrintWriter(new BufferedWriter(new FileWriter("log_MARRC1415.txt", true)));
                impresor.print(logTexto);
                impresor.flush();
                impresor.close();
                logTexto.delete(0, logTexto.length() - 1);
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Devuelve el valor de activo.
     *
     * @return cierto si el log está activo, falso si no lo está.
     */
    public static boolean estaActivo() {
        return activo;
    }

    /**
     * Pone a cierto el valor de activo.
     */
    public static void activar() {
        activo = true;
    }

    /**
     * Pone a falso el valor de activo.
     */
    public static void desactivar() {
        activo = false;
    }
}
