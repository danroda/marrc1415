/*
 * Desarrollo de una pequeña herramienta que permita representar y razonar
 * (realizando inferencias) sobre una Red Bayesiana de una forma sencilla.
 * "Métodos avanzados de razonamiento y representación del conocimiento", Máster
 * en Ingeniería Informática, curso 2014-2015, Universidad de Valladolid.
 * Autores: Borja Cerezo, Cristian Tejedor y Daniel Roda.
 */
package proyecto1415MARRC.redBayesiana;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que modela una tabla-factor para realizar operaciones de
 * multiplicación, sumatorio, normalización y reducción.
 *
 * @author marrc1415
 * @since 0.2-BETA
 * @version 1.0-FINAL
 */
public final class Factor {

    /**
     * Nombre del nodo (único en la misma {@link IRedBayesiana}).
     */
    private List<Nodo> nodos;
    /**
     * Posibles factores del nodo.
     */
    private List<Double> factores;

    /**
     * Constructor privado para que no se pueda crear así.
     */
    private Factor() {
    }

    /**
     * Constructor que crea un factor a través de sus nodos y de posibles
     * factores.
     *
     * @param misNodos Nodos propios.
     * @param misFactores Factores.
     */
    protected Factor(final List<Nodo> misNodos, final List<Double> misFactores) {
        this.nodos = misNodos;
        this.factores = misFactores;
    }

    /**
     * Devuelve la lista de nodos del factor.
     *
     * @return lista de nodos.
     */
    public List<Nodo> getNodos() {
        List<Nodo> misNodos = new ArrayList<Nodo>();
        for (final Nodo nodo : nodos) {
            misNodos.add(nodo);
        }
        return misNodos;
    }

    /**
     * Indica si el factor contiene una variable.
     *
     * @param variable
     * @return cierto si tiene la variable, false si no la tiene.
     */
    protected boolean tieneVariable(final String variable) {
        for (final Nodo nodo : this.nodos) {
            if (nodo.getNombre().equals(variable)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Devuelve el valor de un factor concreto.
     *
     * @param posicion posicion del valor a devolver
     * @return el valor de un factor concreto.
     */
    public Double getFactor(final int posicion) {
        return this.factores.get(posicion);
    }

    /**
     * Devuelve el valor de un factor concreto.
     *
     * @param estadosNodos estados de los nodos para el valor a devolver.
     * @return el valor de un factor concreto.
     */
    public Double getFactor(final List<Integer> estadosNodos) {
        return this.factores.get(estadosToPosicion(estadosNodos));
    }

    /**
     * Realiza el sumatorio de un factor sobre una variable.
     *
     * @param nombreNodo Nombre del nodo a sobre el que se suma.
     * @return Factor obtenido sobre el sumatorio.
     * @throws ExcepcionNodoInexistente si no se posee un nodo con ese nombre en
     * la lista.
     */
    protected Factor sumatorio(final String nombreNodo) throws ExcepcionNodoInexistente {
        final List<String> listaNodos = new ArrayList<String>();
        listaNodos.add(nombreNodo);
        return sumatorio(listaNodos);
    }

    /**
     * Realiza el sumatorio de un factor sobre una variable.
     *
     * @param nombreNodo lista de nombres de los nodos a sobre el que se suma.
     * @return Factor obtenido tras el sumatorio.
     * @throws ExcepcionNodoInexistente si no se posee un nodo con ese nombre en
     * la lista.
     */
    protected Factor sumatorio(final List<String> nombreNodo) throws ExcepcionNodoInexistente {
        if (nombreNodo.size() == 0 || nodos.size() == 0) {
            return this;
        }
        final List<Nodo> nuevoFactorNodos = new ArrayList<Nodo>();
        final List<Double> nuevoFactorFactores = new ArrayList<Double>();
        final List<Integer> pivoteExterno = new ArrayList<Integer>();
        final List<Integer> pivoteInterno = new ArrayList<Integer>();
        final List<Integer> estadoExterno = new ArrayList<Integer>();
        final List<Integer> numEstados = new ArrayList<Integer>();
        boolean error;
        for (final String nombreNodoActual : nombreNodo) {
            error = true;
            for (int i = 0; (i < nodos.size()) && error; i++) {
                if (nodos.get(i).getNombre().equals(nombreNodoActual)) {
                    error = false;
                    pivoteExterno.add(i);
                }
            }
        }
        for (int i = 0; (i < nodos.size()); i++) {
            if (!pivoteExterno.contains(i)) {
                nuevoFactorNodos.add(nodos.get(i));
                pivoteInterno.add(i);
            }
            estadoExterno.add(0);
            numEstados.add(nodos.get(i).getNumEstados());
        }
        BigDecimal factorActual;
        List<Integer> estadoInterno;
        do {
            estadoInterno = new ArrayList<Integer>();
            for (final int i : estadoExterno) {
                estadoInterno.add(i);
            }
            factorActual = BigDecimal.ZERO;
            do {
                factorActual = factorActual.add(BigDecimal.valueOf(this.factores.get(estadosToPosicion(estadoInterno))));
            } while (FuncionesComunes.siguienteCombinacion(estadoInterno, numEstados, pivoteInterno));
            nuevoFactorFactores.add(factorActual.doubleValue());
        } while (FuncionesComunes.siguienteCombinacion(estadoExterno, numEstados, pivoteExterno));
        return new Factor(nuevoFactorNodos, nuevoFactorFactores);
    }

    /**
     * Realiza el producto con otro factor.
     *
     * @param factor factor con el que se va a realizar el producto .
     * @param ordenNombreNodos contiene la ordenación topológica de nodos por su
     * nombre.
     * @return el resultado de la operacion.
     * @throws ExcepcionNodoInexistente si no se posee un nodo con ese nombre en
     * la lista.
     */
    protected Factor multiplica(final Factor factor, final List<String> ordenNombreNodos) throws ExcepcionNodoInexistente {
        final List<Nodo> nuevoFactorNodos = new ArrayList<Nodo>();
        final List<Double> nuevoFactorFactores = new ArrayList<Double>();
        final List<String> nodosPropiosNombre = new ArrayList<String>();
        for (final Nodo nodo : nodos) {
            nodosPropiosNombre.add(nodo.getNombre());
        }
        final List<String> nodosAjenosNombre = new ArrayList<String>();
        for (final Nodo nodo : factor.getNodos()) {
            nodosAjenosNombre.add(nodo.getNombre());
        }
        final List<Integer> estadoActual = new ArrayList<Integer>();
        final List<Integer> tamElementos = new ArrayList<Integer>();
        final List<Integer> posicionesNodosPropios = new ArrayList<Integer>();
        final List<Integer> posicionesNodosAjenos = new ArrayList<Integer>();
        Nodo nodo;
        for (final String nombreNodo : ordenNombreNodos) {
            boolean found = false;
            if (nodosPropiosNombre.contains(nombreNodo)) {
                found = true;
                nodo = nodos.get(nodosPropiosNombre.indexOf(nombreNodo));
                nuevoFactorNodos.add(nodo);
                estadoActual.add(0);
                tamElementos.add(nodo.getNumEstados());
                posicionesNodosPropios.add(((nuevoFactorNodos.size()) - 1));
            }
            if (nodosAjenosNombre.contains(nombreNodo)) {
                if (!found) {
                    nodo = factor.getNodos().get(nodosAjenosNombre.indexOf(nombreNodo));
                    nuevoFactorNodos.add(nodo);
                    estadoActual.add(0);
                    tamElementos.add(nodo.getNumEstados());
                }
                posicionesNodosAjenos.add(((nuevoFactorNodos.size()) - 1));
            }
        }
        List<Integer> estadosPropios, estadosAjenos;
        do {
            estadosPropios = new ArrayList<Integer>();
            for (final int posicion : posicionesNodosPropios) {
                estadosPropios.add(estadoActual.get(posicion));
            }
            estadosAjenos = new ArrayList<Integer>();
            for (final int posicion : posicionesNodosAjenos) {
                estadosAjenos.add(estadoActual.get(posicion));
            }
            nuevoFactorFactores.add(BigDecimal.valueOf(getFactor(estadosPropios)).multiply(BigDecimal.valueOf(factor.getFactor(estadosAjenos))).doubleValue());
        } while (FuncionesComunes.siguienteCombinacion(estadoActual, tamElementos));
        return new Factor(nuevoFactorNodos, nuevoFactorFactores);
    }

    /**
     * Devuelve el factor reducido ante una evidencia.
     *
     * @param evidenciasNombreNodos nombre de los nodos con evidencias.
     * @param evidenciasIdEstados id del estado evidenciado en los nodos con
     * evidencias en el mismo orden que evidenciasNombreNodos.
     * @return Factor reducido.
     */
    protected Factor evidencia(final List<String> evidenciasNombreNodos, final List<Integer> evidenciasIdEstados) {
        final List<Nodo> nuevoFactorNodos = new ArrayList<Nodo>();
        final List<Double> nuevoFactorFactores = new ArrayList<Double>();
        final List<Integer> factorEstados = new ArrayList<Integer>();
        final List<Integer> numEstados = new ArrayList<Integer>();
        final List<Integer> pivotes = new ArrayList<Integer>();
        final int numNodos = nodos.size();
        String nombreNodo;
        for (int idNodo = 0; idNodo < numNodos; idNodo++) {
            numEstados.add(nodos.get(idNodo).getNumEstados());
            nombreNodo = nodos.get(idNodo).getNombre();
            if (evidenciasNombreNodos.contains(nombreNodo)) {
                pivotes.add(idNodo);
                factorEstados.add(evidenciasIdEstados.get(evidenciasNombreNodos.indexOf(nombreNodo)));
            } else {
                nuevoFactorNodos.add(nodos.get(idNodo));
                factorEstados.add(0);
            }
        }
        do {
            nuevoFactorFactores.add(this.factores.get(estadosToPosicion(factorEstados)));
        } while (FuncionesComunes.siguienteCombinacion(factorEstados, numEstados, pivotes));
        return new Factor(nuevoFactorNodos, nuevoFactorFactores);
    }

    /**
     * Devuelve el factor normalizado respecto a alpha.
     *
     * @param alpha valor por el que hay que dividir el factor para normalizar.
     * @return Factor normalizado.
     */
    protected Factor normalizar(final Double alpha) {
        final List<Nodo> nuevoFactorNodos = new ArrayList<Nodo>();
        for (final Nodo nodo : this.nodos) {
            nuevoFactorNodos.add(nodo);
        }
        final List<Double> nuevoFactorFactores = new ArrayList<Double>();
        for (double factorActual : this.factores) {
            nuevoFactorFactores.add(BigDecimal.valueOf(factorActual).divide(BigDecimal.valueOf(alpha), 10, RoundingMode.HALF_UP).doubleValue());
        }
        return new Factor(nuevoFactorNodos, nuevoFactorFactores);
    }

    /**
     * Devuelve el factor ordenado en el orden topológico indicado por nombre.
     *
     * @param ordenNombreNodos contiene la ordenación topológica de nodos por su
     * nombre.
     * @return el resultado de la operacion.
     */
    protected Factor ordena(final List<String> ordenNombreNodos) {
        final List<Nodo> nuevoFactorNodos = new ArrayList<Nodo>();
        final List<Double> nuevoFactorFactores = new ArrayList<Double>();
        final List<String> nodosPropiosNombre = new ArrayList<String>();
        for (final Nodo nodo : this.nodos) {
            nodosPropiosNombre.add(nodo.getNombre());
        }
        final List<Integer> posicionesNodosPropios = new ArrayList<Integer>();
        final List<Integer> estadoActual = new ArrayList<Integer>();
        final List<Integer> tamElementos = new ArrayList<Integer>();
        final List<Integer> estadosPropios = new ArrayList<Integer>();
        int numElementos = 0;
        int posicion;
        Nodo nodo;
        for (final String nombreNodo : ordenNombreNodos) {
            if (nodosPropiosNombre.contains(nombreNodo)) {
                posicion = nodosPropiosNombre.indexOf(nombreNodo);
                nodo = this.nodos.get(posicion);
                nuevoFactorNodos.add(nodo);
                estadoActual.add(0);
                estadosPropios.add(0);
                tamElementos.add(nodo.getNumEstados());
                posicionesNodosPropios.add(posicion);
                numElementos++;
            }
        }
        do {
            for (int i = 0; i < numElementos; i++) {
                estadosPropios.set(posicionesNodosPropios.get(i), estadoActual.get(i));
            }
            nuevoFactorFactores.add(this.getFactor(estadosPropios));
        } while (FuncionesComunes.siguienteCombinacion(estadoActual, tamElementos));
        return new Factor(nuevoFactorNodos, nuevoFactorFactores);
    }

    /**
     * Transforma la posición de un factor en la lista de factores en una lista
     * con los indices de estado de los nodos correspondientes.
     *
     * @param posicion posición de un factor en la lista de factores.
     * @return Lista con los indices de estado de los nodos correspondientes.
     */
    public List<Integer> posicionToEstados(int posicion) {
        final List<Integer> estadosNodos = new ArrayList<Integer>();
        int nEstadosAux;
        final int indiceUltimoNodo = (nodos.size() - 1);
        for (int i = indiceUltimoNodo; i > -1; i--) {
            nEstadosAux = nodos.get(i).getNumEstados();
            estadosNodos.add(posicion % nEstadosAux);
            posicion = posicion / nEstadosAux;
        }
        return estadosNodos;
    }

    /**
     * Transforma una lista con los indices de estado de los nodos en la
     * posición del elemento el la lista de factores.
     *
     * @param estadosNodos Lista de indices de indices de estado de los nodos en
     * el orden indexado de la lista de nodos.
     * @return posición del factor correspondente a esos estados en la lista de
     * factores.
     */
    public int estadosToPosicion(final List<Integer> estadosNodos) {
        int base = 1;
        int posicion = 0;
        final int indiceUltimoNodo = (nodos.size() - 1);
        for (int i = indiceUltimoNodo; i > -1; i--) {
            posicion += base * estadosNodos.get(i);
            base *= nodos.get(i).getNumEstados();
        }
        return posicion;
    }
}