/*
 * Desarrollo de una pequeña herramienta que permita representar y razonar
 * (realizando inferencias) sobre una Red Bayesiana de una forma sencilla.
 * Métodos avanzados de razonamiento y representación del conocimiento, Máster
 * en Ingeniería Informática, curso 2014-2015, Universidad de Valladolid.
 * Autores: Borja Cerezo, Cristian Tejedor y Daniel Roda.
 */
package proyecto1415MARRC.redBayesiana;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * Clase entidad que modela un nodo de una red.
 *
 * @author marrc1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public class Nodo extends Observable {

    /**
     * Nombre del nodo (único en la misma {@link IRedBayesiana}).
     */
    private String nombre;
    /**
     * ID del nodo (único en la misma {@link IRedBayesiana}).
     */
    private int id;
    /**
     * Lista de nodos padres del nodo.
     */
    private List<Nodo> padres;
    /**
     * Lista de hijos del nodo.
     */
    private List<Nodo> hijos;
    /**
     * Lista de nombres de estados del nodo.
     */
    private List<String> estados;
    /**
     * Indice del estado al que se ha fijado el nodo en la lista de estados si
     * lo hay.
     */
    private int estadoFijado;
    /**
     * Tabla de probabilidad condicionada del nodo. Las columnas son los
     * distintos estados del nodo, y las filas las distintas combinaciones de
     * estados del padre.
     */
    private List<ArrayList<Double>> dPC;
    /**
     * Número de índice que indica una posición fuera de la lista.
     */
    private static byte NO_INDICE = -1;

    /**
     * Constructor privado para que no se pueda crear así.
     */
    private Nodo() {
    }

    /**
     * Constructor único de un nodo.
     *
     * @param nombreNodo Nombre del nodo a crear.
     * @param idNodo Posición que ocupa el nodo para RedBayesiana.
     */
    protected Nodo(final String nombreNodo, final int idNodo) {
        this.nombre = nombreNodo;
        this.id = idNodo;
        this.padres = new ArrayList<Nodo>();
        this.hijos = new ArrayList<Nodo>();
        this.dPC = new ArrayList<ArrayList<Double>>();
        this.estados = new ArrayList<String>();
        this.estadoFijado = ConstantesObservador.NO_EVIDENCIA;
    }

    /**
     * Calcula el valor del ID del nodo y se lo establece.
     *
     * @param red a la que peretenece el nodo.
     * @throws ExcepcionNodoInexistente si no se encuentra en la lista de la
     * red.
     */
    protected final void setId(final IRedBayesiana red) throws ExcepcionNodoInexistente {
        int numNodos = red.numNodos();
        for (int i = 0; i < numNodos; i++) {
            if (red.getNodo(i).equals(this)) {
                this.id = i;
                numNodos = NO_INDICE;
            }
        }
        if (numNodos != NO_INDICE) {
            throw new ExcepcionNodoInexistente("El nodo " + this.id
                    + "no se localiza a sí mismo en la red bayesiana para fijar su id.");
        }
    }

    /**
     * Establece una relación con otro nodo como su hijo.
     *
     * @param padre Padre a añadir al nodo.
     * @throws ExcepcionDPCExistente Si ya existe una relación entre ambos
     * nodos.
     */
    protected void addPadre(final Nodo padre) throws ExcepcionDPCExistente {
        if (this.tieneDPC()) {
            throw new ExcepcionDPCExistente("No se puede establecer la relación '"
                    + padre.getNombre() + " -> " + this.nombre + "' porque "
                    + this.nombre + " tiene DPC.");
        }
        if (!(padres.contains(padre))) {
            this.padres.add(padre);
        }
        if (padre.getHijos().contains(this)) {
            this.setChanged();
            this.notifyObservers(new Integer[]{ConstantesObservador.ACTUALIZA_RELACION, padre.getId(), this.id, 1});
        } else {
            padre.addHijo(this);
        }
    }

    /**
     * Elimina la relación con un padre sabiendo su nombre.
     *
     * @param nombrePadre Nombre del nodo padre.
     * @throws ExcepcionDPCExistente Si existe una DPC que quedaría inutil de
     * eliminar el nodo.
     * @throws ExcepcionNodoInexistente Si no se encuentra un padre con ese
     * nombre.
     */
    protected void deletePadre(final String nombrePadre) throws ExcepcionDPCExistente,
            ExcepcionNodoInexistente {
        deletePadreIndice(nombrePadreToIndice(nombrePadre));
    }

    /**
     * Elimina la relación con un padre sabiendo su id.
     *
     * @param idPadre id del nodo padre.
     * @throws ExcepcionDPCExistente Si existe una DPC que quedaría inutil de
     * eliminar la relación.
     * @throws ExcepcionNodoInexistente Si no se encuentra un padre con ese id.
     */
    protected void deletePadre(final int idPadre) throws ExcepcionDPCExistente,
            ExcepcionNodoInexistente {
        deletePadreIndice(idPadreToIndice(idPadre));
    }

    /**
     * Elimina la relación con padres[indicePadre].
     *
     * @param indicePadre Índice del nodo padre.
     * @throws ExcepcionDPCExistente Si existe una DPC que quedaría inutil de
     * eliminar la relación.
     * @throws ExcepcionNodoInexistente Si no se encuentra un padre con ese
     * índice.
     */
    protected void deletePadreIndice(final int indicePadre) throws ExcepcionDPCExistente,
            ExcepcionNodoInexistente {
        if (this.tieneDPC()) {
            throw new ExcepcionDPCExistente("No se puede eliminar la relación '"
                    + padres.get(indicePadre).getNombre() + " -> " + this.nombre + "' porque " + this.nombre + " tiene DPC");
        }
        if ((indicePadre < 0) || (indicePadre >= getNumPadres())) {
            throw new ExcepcionNodoInexistente("El nodo " + this.nombre
                    + " no tiene un padre que borrar en la posición " + indicePadre + ".");
        }
        this.padres.remove(indicePadre);
        if (padres.get(indicePadre).getHijos().contains(this)) {
            this.setChanged();
            this.notifyObservers(new Integer[]{padres.get(indicePadre).getId(),
                this.id, 0});
        } else {
            padres.get(indicePadre).deleteHijo(this.id);
        }
    }

    /**
     * Añade un hijo nodo al nodo.
     *
     * @param hijo Hijo a añadir al nodo.
     * @throws ExcepcionDPCExistente Si existe una DPC que quedaría inutil de
     * establecer la relación.
     */
    protected void addHijo(final Nodo hijo) throws ExcepcionDPCExistente {
        if (hijo.tieneDPC()) {
            throw new ExcepcionDPCExistente("No se puede establecer la relación '"
                    + this.nombre + " -> " + hijo.getNombre() + "' porque " + hijo.getNombre() + " tiene DPC.");
        }
        if (!(hijos.contains(hijo))) {
            this.hijos.add(hijo);
        }
        if (hijo.getPadres().contains(this)) {
            this.setChanged();
            this.notifyObservers(new Integer[]{ConstantesObservador.ACTUALIZA_RELACION, this.id, hijo.getId(), 1});
        } else {
            hijo.addPadre(this);
        }
    }

    /**
     * Elimina la relación con un hijo sabiendo su nombre.
     *
     * @param nombreHijo nombre del nodo hijo.
     * @throws ExcepcionDPCExistente Si existe una DPC que quedaría inutil de
     * eliminar la relación.
     * @throws ExcepcionNodoInexistente Si no se encuentra un hijo con ese
     * nombre.
     */
    protected void deleteHijo(final String nombreHijo) throws ExcepcionDPCExistente,
            ExcepcionNodoInexistente {
        deleteHijoIndice(nombreHijoToIndice(nombreHijo));
    }

    /**
     * Elimina la relación con un hijo sabiendo su id.
     *
     * @param idHijo id del nodo hijo.
     * @throws ExcepcionDPCExistente Si existe una DPC que quedaría inutil de
     * eliminar la relación.
     * @throws ExcepcionNodoInexistente Si no se encuentra un hijo con ese id.
     */
    protected void deleteHijo(final int idHijo) throws ExcepcionDPCExistente, ExcepcionNodoInexistente {
        deletePadreIndice(idPadreToIndice(idHijo));
    }

    /**
     * Elimina la relación con hijos[indiceHijo].
     *
     * @param indiceHijo id del nodo hijo.
     * @throws ExcepcionDPCExistente Si existe una DPC que quedaría inutil de
     * eliminar la relación.
     * @throws ExcepcionNodoInexistente Si no se encuentra un hijo con ese
     * índice.
     */
    protected void deleteHijoIndice(final int indiceHijo) throws ExcepcionDPCExistente, ExcepcionNodoInexistente {
        if (hijos.get(indiceHijo).tieneDPC()) {
            throw new ExcepcionDPCExistente("No se puede establecer la relación '"
                    + this.nombre + " -> " + hijos.get(indiceHijo).getNombre() + "' porque " + hijos.get(indiceHijo).getNombre() + " tiene DPC.");
        }
        if ((indiceHijo < 0) || (indiceHijo >= getNumHijos())) {
            throw new ExcepcionNodoInexistente("El nodo " + this.nombre
                    + " no tiene un hijo que borrar en la posición " + indiceHijo + ".");
        }
        this.hijos.remove(indiceHijo);
        if (hijos.get(indiceHijo).getPadres().contains(this)) {
            this.setChanged();
            this.notifyObservers(new Integer[]{ConstantesObservador.ACTUALIZA_RELACION, this.id, hijos.get(indiceHijo).getId(), 0});
        } else {
            padres.get(indiceHijo).deletePadre(this.id);
        }
    }

    /**
     * Determina los posibles estados del Nodo.
     *
     * @param estados lista los estados que va a tener el Nodo.
     * @throws ExcepcionDPCExistente Si existe una DPC que quedaría inutil de
     * modificar la lista de estados del Nodo.
     * @throws ExcepcionEstadoDuplicado si la lista incluye más de un estado con
     * el mismo nombre
     */
    protected void setEstados(final List<String> estados) throws ExcepcionDPCExistente,
            ExcepcionEstadoDuplicado {
        if (this.tieneDPC() || this.hijoTieneDPC()) {
            throw new ExcepcionDPCExistente("No se pueden fijar los estados del nodo "
                    + this.nombre + " porque ya tiene DPC.");
        }
        this.estados = new ArrayList<String>();
        for (final String estado : estados) {
            addEstado(estado);
        }
    }

    /**
     * Fija el estado del Nodo.
     *
     * @param indiceEstado entero correspondiente al indice del estado en la
     * lista de estados del nodo que se fija.
     * @throws ExcepcionEstadoInexistente Si no existe el estado solicitado en
     * la lista de estados del Nodo.
     */
    protected void setEstadoFijado(final int indiceEstado) throws ExcepcionEstadoInexistente {
        if (indiceEstado < 0 || indiceEstado >= getNumEstados()) {
            throw new ExcepcionEstadoInexistente("No se encuentra el estado con indice "
                    + indiceEstado + " en el nodo " + this.nombre + ".");
        } else {
            this.estadoFijado = indiceEstado;
            this.setChanged();
            this.notifyObservers(new Integer[]{ConstantesObservador.ACTUALIZA_EVIDENCIA, this.id, this.estadoFijado});
        }
    }

    /**
     * Fija el estado del Nodo.
     *
     * @param nombreEstado nombre del estado en la lista de estados del nodo que
     * se fija.
     * @throws ExcepcionEstadoInexistente Si no existe el estado solicitado en
     * la lista de estados del Nodo.
     */
    protected void setEstadoFijado(final String nombreEstado) throws ExcepcionEstadoInexistente {
        this.estadoFijado = this.nombreEstadoToIndice(nombreEstado);
        this.setChanged();
        this.notifyObservers(new Integer[]{ConstantesObservador.ACTUALIZA_EVIDENCIA, this.id, this.estadoFijado});
    }

    /**
     * Elimina evidencia sin fijar el estado del Nodo.
     */
    protected void clearEstadoFijado() {
        this.estadoFijado = ConstantesObservador.NO_EVIDENCIA;
        this.setChanged();
        this.notifyObservers(new Integer[]{ConstantesObservador.ACTUALIZA_EVIDENCIA, this.id, this.estadoFijado});
    }

    /**
     * Añade un estado a la lista de estados del Nodo.
     *
     * @param estado nombre del nuevo estado.
     * @throws ExcepcionDPCExistente Si existe una DPC que quedaría inutil de
     * modificar la lista de estados del Nodo.
     * @throws ExcepcionEstadoDuplicado si la lista incluye más de un estado con
     * el mismo nombre
     */
    protected void addEstado(final String estado) throws ExcepcionDPCExistente, ExcepcionEstadoDuplicado {
        if (this.estados.contains(estado)) {
            throw new ExcepcionEstadoDuplicado("El nodo " + this.nombre + " ya contiene un estado nombrado "
                    + estado + ".");
        }
        if (this.tieneDPC() || this.hijoTieneDPC()) {
            throw new ExcepcionDPCExistente("No se pueden añadir el estado "
                    + estado + " al nodo " + this.nombre + " porque ya tiene DPC.");
        }
        this.estados.add(estado);

    }

    /**
     * Elimina un estado del nodo conocido su nombre.
     *
     * @param estado nombre del estado a eliminar del -nodo.
     * @throws ExcepcionDPCExistente Si existe una DPC que quedaría inutil de
     * modificar la lista de estados del Nodo.
     * @throws ExcepcionEstadoInexistente Si no existe el estado que se desea
     * eliminar.
     */
    protected void deleteEstado(final String estado) throws ExcepcionDPCExistente,
            ExcepcionEstadoInexistente {
        if (!(this.estados.contains(estado))) {
            throw new ExcepcionEstadoInexistente("No hay ningún estado " + estado
                    + " en el nodo " + this.nombre + " para eliminar.");
        }
        if (this.tieneDPC() || this.hijoTieneDPC()) {
            throw new ExcepcionDPCExistente("No se pueden añadir el estado "
                    + estado + " al nodo " + this.nombre + " porque ya tiene DPC.");
        }
        this.estados.remove(estado);

    }

    /**
     * Elimina un estado conocido su índice en la lista del Nodo.
     *
     * @param estado indica el indice del estado en la lista del Nodo.
     * @throws ExcepcionDPCExistente Si existe una DPC que quedaría inutil de
     * modificar la lista de estados del Nodo.
     * @throws ExcepcionEstadoInexistente Si no existe el estado que se desea
     * eliminar.
     */
    protected void deleteEstado(final int estado) throws ExcepcionDPCExistente, ExcepcionEstadoInexistente {
        if (estado < 0 || estado >= this.estados.size()) {
            throw new ExcepcionEstadoInexistente("No hay ningún estado " + estado
                    + " en el nodo " + this.nombre + " para eliminar.");
        }
        if (this.tieneDPC() || this.hijoTieneDPC()) {
            throw new ExcepcionDPCExistente("No se pueden añadir el estado "
                    + estado + " al nodo " + this.nombre + " porque ya tiene DPC.");
        }
        this.estados.remove(estado);

    }

    /**
     * Fija la tabla de distribución de probabilidad condicional del Nodo.
     *
     * @param dPC Tabla de probabilidad condicional a almacnar.
     * @throws ExcepcionDPCIncoherente Si la DPC que se recibe es inválida por
     * algún motivo.
     */
    protected void setTablaDPC(final List<ArrayList<Double>> dPC) throws ExcepcionDPCIncoherente {
        final int numColumnasCorrecto = this.getNumEstados();
        int numFilasCorrecto = 1;
        for (final Nodo nodoPadre : this.padres) {
            numFilasCorrecto *= nodoPadre.getNumEstados();
        }
        if (dPC.size() != numFilasCorrecto) {
            throw new ExcepcionDPCIncoherente("La tabla DPC del nodo " + this.nombre + " ha de tener "
                    + numFilasCorrecto + " filas y no " + dPC.size() + ".");
        }
        ArrayList<Double> fila;
        BigDecimal probabilidadTotal;
        for (int i = 0; i < numFilasCorrecto; i++) {
            fila = dPC.get(i);
            if (fila.size() != numColumnasCorrecto) {
                throw new ExcepcionDPCIncoherente("La tabla DPC del nodo " + this.nombre + " ha de tener "
                        + numColumnasCorrecto + " columnas.\nSe intentan poner "
                        + fila.size() + "en la fila " + i + 1 + ".");
            }
            probabilidadTotal = new BigDecimal(0);
            for (final double probabilidad : fila) {
                probabilidadTotal = probabilidadTotal.add(new BigDecimal(probabilidad));
            }
            if (probabilidadTotal.doubleValue() != 1) {
                throw new ExcepcionDPCIncoherente("Las filas de las tablas DPC han de sumar 1. La fila "
                        + (i + 1) + " del nodo " + this.nombre + " suma "
                        + probabilidadTotal + ".");
            }
        }
        this.dPC = dPC;
    }

    /**
     * Elimina la tabla de probabilidad condicionada.
     *
     */
    protected void deleteTablaDPC() {
        this.dPC = null;
    }

    /**
     * Devuelve el valor del nombre del Nodo.
     *
     * @return nombre del nodo.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Devuelve el valor de la id del Nodo.
     *
     * @return id del nodo.
     */
    public int getId() {
        return id;
    }

    /**
     * Devuelve la lista de padres del nodo.
     *
     * @return lista de padres del nodo.
     */
    public List<Nodo> getPadres() {
        return padres;
    }

    /**
     * Devuelve el número de padres del Nodo.
     *
     * @return número de padres del nodo.
     */
    public int getNumPadres() {
        return padres.size();
    }

    /**
     * Transforma el nombre de un padre en su índice correspondiente en la lista
     * de padres, o devuelve NO_INDICE si no se encuentra en la lista de padres.
     *
     * @param nombrePadre nombre de un padre.
     * @return índice del padre solicitado en la lista de padres.
     */
    public int nombrePadreToIndice(final String nombrePadre) {
        for (final Nodo nodoPadre : this.padres) {
            if (nodoPadre.getNombre().equals(nombrePadre)) {
                return padres.indexOf(nodoPadre);
            }
        }
        return NO_INDICE;
    }

    /**
     * Transforma el id de un padre en su índice correspondiente en la lista de
     * padres, o devuelve NO_INDICE si no se encuentra en la lista de padres.
     *
     * @param idPadre id de un padre.
     * @return índice del padre solicitado en la lista de padres.
     */
    public int idPadreToIndice(final int idPadre) {
        for (final Nodo nodoPadre : this.padres) {
            if (nodoPadre.getId() == idPadre) {
                return padres.indexOf(nodoPadre);
            }
        }
        return NO_INDICE;
    }

    /**
     * Devuelve la lista de hijos del nodo.
     *
     * @return lista de hijos del nodo.
     */
    public List<Nodo> getHijos() {
        return hijos;
    }

    /**
     * Devuelve el número de hijos del Nodo.
     *
     * @return número de hijos del nodo.
     */
    public int getNumHijos() {
        return padres.size();
    }

    /**
     * Transforma el nombre de un hijo en su índice correspondiente en la lista
     * de hijos, o devuelve NO_INDICE si no se encuentra en la lista de hijos.
     *
     * @param nombreHijo nombre de un hijo.
     * @return índice del padre solicitado en la lista de hijos.
     */
    public int nombreHijoToIndice(final String nombreHijo) {
        for (final Nodo nodoHijo : this.hijos) {
            if (nodoHijo.getNombre().equals(nombreHijo)) {
                return padres.indexOf(nodoHijo);
            }
        }
        return NO_INDICE;
    }

    /**
     * Transforma el id de un hijo en su índice correspondiente en la lista de
     * hijos, o devuelve NO_INDICE si no se encuentra en la lista de hijos.
     *
     * @param idHijo nombre de un hijo.
     * @return índice del padre solicitado en la lista de hijos.
     */
    public int idHijoToIndice(final int idHijo) {
        for (final Nodo nodoHijo : this.hijos) {
            if (nodoHijo.getId() == idHijo) {
                return padres.indexOf(nodoHijo);
            }
        }
        return NO_INDICE;
    }

    /**
     * Devuelve el número una lista con los posibles estados del Nodo.
     *
     * @return lista de estados del nodo.
     */
    public List<String> getEstados() {
        return estados;
    }

    /**
     * Devuelve el número de estados del Nodo.
     *
     * @return numero de estados del nodo.
     */
    public int getNumEstados() {
        return estados.size();
    }

    /**
     * Transforma el nombre de un estado en su índice en la lista de estados.
     *
     * @param nombreEstado nombre de un estado del nodo.
     * @return indice del estado solicitado en la lista de estados del nodo.
     * @throws ExcepcionEstadoInexistente Si no se localiza un estado en la
     * lista de estados del nodo con el nombre solicitado.
     */
    public int nombreEstadoToIndice(final String nombreEstado) throws ExcepcionEstadoInexistente {
        if (estados.contains(nombreEstado)) {
            return estados.indexOf(nombreEstado);
        } else {
            throw new ExcepcionEstadoInexistente("No se encuentra el estado "
                    + nombreEstado + " en el nodo " + this.nombre + ".");
        }
    }

    /**
     * Transforma el indice de un estado en la lista de estados en su nombre
     * correspondiente.
     *
     * @param indiceEstado indice de un estado en la lista de estados del nodo.
     * @return nombre de un estado del nodo solicitado.
     * @throws ExcepcionEstadoInexistente Si no se localiza un estado en la
     * lista de estados del nodo con el indice solicitado.
     */
    public String indiceEstadoToNombre(final int indiceEstado) throws ExcepcionEstadoInexistente {
        if (indiceEstado < 0 || indiceEstado >= getNumEstados()) {
            throw new ExcepcionEstadoInexistente("No se encuentra el estado con indice "
                    + indiceEstado + " en el nodo " + this.nombre + ".");

        } else {
            return estados.get(indiceEstado);
        }
    }

    /**
     * Indica si hay estado Fijado en el Nodo
     *
     * @return Cierto si hay estado fijado, falso si no lo hay.
     */
    public boolean tieneEstadoFijado() {
        return (estadoFijado != ConstantesObservador.NO_EVIDENCIA);
    }

    /**
     * Devuelve el nombre del estado Fijado en el Nodo.
     *
     * @return Nombre del estado fijado.
     * @throws ExcepcionEvidenciaInexistente Si no hay estado fijado.
     */
    public String estadoFijadoToNombre() throws ExcepcionEvidenciaInexistente {
        if (!tieneEstadoFijado()) {
            throw new ExcepcionEvidenciaInexistente("El nodo " + nombre + "no posee estado fijado.");
        }
        return estados.get(estadoFijado);
    }

    /**
     * Devuelve el id estado Fijado en el Nodo.
     *
     * @return id del estado fijado.
     * @throws ExcepcionEvidenciaInexistente Si no hay estado fijado.
     */
    public int estadoFijadoToId() throws ExcepcionEvidenciaInexistente {
        if (!tieneEstadoFijado()) {
            throw new ExcepcionEvidenciaInexistente("El nodo " + nombre + "no posee estado fijado.");
        }
        return estadoFijado;
    }

    /**
     * Devuelve la matriz completa de distribuciones de probabilidad
     * condicionada del nodo tal y como la tiene almacenada.
     *
     * @return Matriz de tabla de distribución de probabilidad condicionada.
     * @throws ExcepcionDPCInexistente Si no existe el nodo no posee tabla de
     * distribución de probabilidad.
     */
    protected List<ArrayList<Double>> getTablaDPC() throws ExcepcionDPCInexistente {
        if (tieneDPC()) {
            final List<ArrayList<Double>> tabla = new ArrayList<ArrayList<Double>>();
            ArrayList<Double> linea;
            for (final ArrayList<Double> lineaOriginal : dPC) {
                linea = new ArrayList<Double>();
                for (double valor : lineaOriginal) {
                    linea.add(valor);
                }
                tabla.add(linea);
            }
            return tabla;
        }
        throw new ExcepcionDPCInexistente("El nodo " + this.nombre
                + " no tiene tabla de distribución de probabilidad condicionada.");

    }

    /**
     * Devuelve la probabilidad de que el Nodo esté en un estado concreto
     * condicionado a que sus padres estén en los estados indicados.
     *
     * @param estadoNodoNombre nombre del estado del nodo.
     * @param estadosPadresNombre lista de nombres de estado de cada padre
     * indexados en el mismo orden que la lista de padres.
     * @return Probabilidad condicionada de ese estado del nodo.
     * @throws ExcepcionPadresDPC Si el numero de estados de padre entregados no
     * es igual al número de padres del nodo.
     * @throws ExcepcionEstadoInexistente Si alguno de los nombres de estado
     * indicados no existen en el nodo o sus padres.
     * @throws ExcepcionDPCInexistente Si no existe el nodo no posee tabla de
     * distribución de probabilidad condicionada.
     */
    public double getDPC(final String estadoNodoNombre, final List<String> estadosPadresNombre)
            throws ExcepcionPadresDPC, ExcepcionEstadoInexistente, ExcepcionDPCInexistente {
        if (!(tieneDPC())) {
            throw new ExcepcionDPCInexistente("El nodo " + this.nombre + " no tiene tabla de distribución de probabilidad condicionada.");
        }
        if (getNumPadres() == estadosPadresNombre.size()) {
            final int indiceEstadoActualNodo = estados.indexOf(estadoNodoNombre);
            final List<Integer> indiceEstadoActualPadres = new ArrayList<Integer>();
            final int numPadres = getNumPadres();
            for (int i = 0; i < numPadres; i++) {
                indiceEstadoActualPadres.add(this.padres.get(i).nombreEstadoToIndice(estadosPadresNombre.get(i)));
            }
            return getDPC(indiceEstadoActualNodo, indiceEstadoActualPadres);
        }
        throw new ExcepcionPadresDPC(this.nombre + " tiene " + getNumPadres()
                + " estados y no " + estadosPadresNombre.size() + ".");

    }

    /**
     * Devuelve la probabilidad de que el Nodo esté en un estado concreto
     * condicionado a que sus padres estén en los estados indicados.
     *
     * @param estadoNodoIndice indice del estado del nodo en la lista de
     * estados.
     * @param estadosPadresIndice lista de indices de estado de cada padre en su
     * respectiva lista de estados indexados en el mismo orden que la lista de
     * padres.
     * @return Probabilidad condicionada de ese estado del nodo.
     * @throws ExcepcionPadresDPC Si el numero de estados de padre entregados no
     * es igual al número de padres del nodo.
     * @throws ExcepcionDPCInexistente Si no existe el nodo no posee tabla de
     * distribución de probabilidad.
     */
    public double getDPC(final int estadoNodoIndice, final List<Integer> estadosPadresIndice)
            throws ExcepcionPadresDPC, ExcepcionDPCInexistente {
        if (!(tieneDPC())) {
            throw new ExcepcionDPCInexistente("El nodo " + this.nombre + " no tiene tabla de distribución de probabilidad condicionada.");
        }
        if (getNumPadres() == estadosPadresIndice.size()) {
            int estadoPadres = padresToFila(estadosPadresIndice);
            return dPC.get(estadoPadres).get(estadoNodoIndice);
        }
        throw new ExcepcionPadresDPC(this.nombre + " tiene " + getNumPadres()
                + " estados y no " + estadosPadresIndice.size() + ".");

    }

    /**
     * Devuelve la probabilidad de que el Nodo esté en un estado concreto para
     * nodos sin padres.
     *
     * @param estadoNodoNombre nombre del estado del nodo.
     * @return Probabilidad condicionada de ese estado del nodo.
     * @throws ExcepcionPadresDPC Si el nodo tiene padres.
     * @throws ExcepcionDPCInexistente Si no existe el nodo no posee tabla de
     * distribución de probabilidad.
     */
    public double getDPC(final String estadoNodoNombre) throws ExcepcionPadresDPC, ExcepcionDPCInexistente {
        if (!(tieneDPC())) {
            throw new ExcepcionDPCInexistente("El nodo " + this.nombre + " no tiene tabla de distribución de probabilidad condicionada.");
        }
        if (getNumPadres() == 0) {
            return getDPC(estados.indexOf(estadoNodoNombre));
        }
        throw new ExcepcionPadresDPC("Se necesita el estado de los padres de "
                + this.nombre + " para dar la DPC.");
    }

    /**
     * Devuelve la probabilidad de que el Nodo esté en un estado concreto para
     * nodos sin padres.
     *
     * @param estadoNodoIndice indice del estado del nodo.
     * @return Probabilidad condicionada de ese estado del nodo.
     * @throws ExcepcionPadresDPC Si el nodo tiene padres.
     * @throws ExcepcionDPCInexistente Si no existe el nodo no posee tabla de
     * distribución de probabilidad.
     */
    public double getDPC(final int estadoNodoIndice) throws ExcepcionPadresDPC, ExcepcionDPCInexistente {
        if (!(tieneDPC())) {
            throw new ExcepcionDPCInexistente("El nodo " + this.nombre + " no tiene tabla de distribución de probabilidad condicionada.");
        }
        if (getNumPadres() == 0) {
            return dPC.get(0).get(estadoNodoIndice);
        }
        throw new ExcepcionPadresDPC("Se necesita el estado de los padres de "
                + this.nombre + " para dar la DPC.");
    }

    /**
     * Indica si el nodo tiene tabla de probabilidad condicionada.
     *
     * @return true si tiene tabla, false si no la tiene.
     */
    public boolean tieneDPC() {
        return (dPC.size() > 0);
    }

    /**
     * Transforma el número de fila de la tabla de probabilidad condicionada en
     * una lista con los indices de estado de los padres correspondiente a esa
     * fila.
     *
     * @param fila numero de fila de la tabla que se pretende transformar.
     * @return Lista de indices de estados de los padres en el orden indexado de
     * la lista de padres.
     */
    private List<Integer> filaToPadres(int fila) {
        final List<Integer> estadosPadres = new ArrayList<Integer>();
        int nEstadosAux;
        final int idUltimoPadre = getNumPadres() - 1;
        for (int i = idUltimoPadre; i > -1; i--) {
            nEstadosAux = padres.get(i).getNumEstados();
            estadosPadres.add(fila % nEstadosAux);
            fila = fila / nEstadosAux;
        }
        return estadosPadres;
    }

    /**
     * Transforma una lista con los indices de estado de los padres en el el
     * número de fila de la tabla de probabilidad condicionada a la que se
     * corresponde.
     *
     * @param estadosPadres Lista de indices de estados de los padres en el
     * orden indexado de la lista de padres.
     * @return numero de fila correspondiente de la tabla de probabilidad.
     */
    private int padresToFila(final List<Integer> estadosPadres) {
        int base = 1;
        int fila = 0;
        final int idUltimoPadre = getNumPadres() - 1;
        for (int i = idUltimoPadre; i > -1; i--) {
            fila += base * estadosPadres.get(i);
            base *= padres.get(i).getNumEstados();
        }
        return fila;
    }

    /**
     * Indica si algún hijo del nodo tiene tabla de probabilidad condicionada.
     *
     * @return true si existe algún hijo con tabla de probabilidad condicionada,
     * false si ninguno tiene.
     */
    public boolean hijoTieneDPC() {
        for (final Nodo nodoHijo : this.hijos) {
            if (nodoHijo.tieneDPC()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Factor.
     *
     * @return Factor correspondiente al Nodo.
     * @throws ExcepcionDPCInexistente Si el Nodo no tiene tabla de distribución
     * de probabilidad condicionada.
     */
    public Factor factor() throws ExcepcionDPCInexistente {
        if (tieneDPC()) {
            final List<Nodo> factorNodos = new ArrayList<Nodo>();
            final List<Double> factorFactores = new ArrayList<Double>();
            factorNodos.add(this);
            for (final Nodo nodo : this.padres) {
                factorNodos.add(nodo);
            }
            int tamFila = dPC.get(0).size();
            int tamColumna = dPC.size();
            for (int j = 0; j < tamFila; j++) {
                for (int i = 0; i < tamColumna; i++) {
                    factorFactores.add(dPC.get(i).get(j));
                }
            }
            return new Factor(factorNodos, factorFactores);
        }
        throw new ExcepcionDPCInexistente("El nodo " + this.nombre
                + " no tiene tabla de distribución de probabilidad condicionada.");
    }
}