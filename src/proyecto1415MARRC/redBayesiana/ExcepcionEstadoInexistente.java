/*
 * Desarrollo de una pequeña herramienta que permita representar y razonar
 * (realizando inferencias) sobre una Red Bayesiana de una forma sencilla.
 * Métodos avanzados de razonamiento y representación del conocimiento, Máster
 * en Ingeniería Informática, curso 2014-2015, Universidad de Valladolid.
 * Autores: Borja Cerezo, Cristian Tejedor y Daniel Roda.
 */
package proyecto1415MARRC.redBayesiana;

/**
 * Excepción ocurrida cuando se intenta localizar un estado de que no existe en
 * un nodo.
 *
 * @author marrc1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public final class ExcepcionEstadoInexistente extends Exception {

    /**
     * Mensaje por defecto de la excepción.
     */
    private final static String MENSAJE = "Se ha intentado acceder a un estado que no existe en un nodo.";

    /**
     * Constructor por defecto que muestra el mensaje explicatorio.
     */
    public ExcepcionEstadoInexistente() {
        super(MENSAJE);
    }

    /**
     * Muestra un mensaje de error extra al mensaje explicatorio por defecto.
     *
     * @param msg Message error.
     */
    public ExcepcionEstadoInexistente(final String msg) {
        super(MENSAJE + '\n' + msg);
    }
}