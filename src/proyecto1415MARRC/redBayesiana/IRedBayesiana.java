/*
 * Desarrollo de una pequeña herramienta que permita representar y razonar
 * (realizando inferencias) sobre una Red Bayesiana de una forma sencilla.
 * Métodos avanzados de razonamiento y representación del conocimiento, Máster
 * en Ingeniería Informática, curso 2014-2015, Universidad de Valladolid.
 * Autores: Borja Cerezo, Cristian Tejedor y Daniel Roda.
 */
package proyecto1415MARRC.redBayesiana;

import java.util.ArrayList;
import java.util.List;

/**
 * Interfaz que modela una red bayesiana.
 *
 * @author marrc1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public interface IRedBayesiana {

    // 1. Funciones de "Dar valores"
    /**
     * Añade un nodo a la red con el nombre especificado.
     *
     * @param nombreNodo nombre del nuevo nodo.
     * @throws ExcepcionNodoDuplicado Si ya existe un nodo en la red con ese
     * nombre.
     */
    public void addNodo(final String nombreNodo) throws ExcepcionNodoDuplicado;

    /**
     * Elimina el Nodo de la red indicado por su nombre.
     *
     * @param nombreNodo nombre del nodo a eliminar.
     * @throws ExcepcionNodoInexistente Si no existe en la red un nodo con ese
     * nombre.
     * @throws ExcepcionDPCExistente Si existe una tabla de probabilidad
     * condicionada que quedaría inutilizada de eliminar el nodo.
     */
    public void deleteNodo(final String nombreNodo) throws ExcepcionNodoInexistente, ExcepcionDPCExistente;

    /**
     * Elimina el Nodo de la red indicado por su id.
     *
     * @param idNodo id del nodo.
     * @throws ExcepcionNodoInexistente Si no existe en la red un nodo con ese
     * id.
     * @throws ExcepcionDPCExistente Si existe una tabla de distribución de
     * probabilidad condicionada que quedaría inutilizada de eliminar el nodo.
     */
    public void deleteNodo(final int idNodo) throws ExcepcionNodoInexistente, ExcepcionDPCExistente;

    /**
     * Establece las relaciones de la red de acuerdo a una matriz de adyacencia.
     *
     * @param adyacencia matriz de adyacencia que modela las relaciones de la
     * red.
     * @throws ExcepcionDPCExistente Si existe alguna tabla de probabilidad
     * condicionada en algún nodo que quedaría inutilizada de establecerse la
     * nueva red de relaciones.
     * @throws ExcepcionMatrizAdyacenciaIncoherente Si se detectan errores de
     * formato o incoherencias en la matriz de adyacencia recibida.
     * @throws ExcepcionNodoInexistente Si al ir recorriendo los nodos en las
     * consultas se encuentra uno que no existe.
     * @throws ExcepcionGrafoCiclico Si la matriz de adyacencia introducida es
     * correcta en formato y coherente pero presenta bucles.
     */
    public void setAdyacencia(final List<ArrayList<Integer>> adyacencia) throws ExcepcionDPCExistente, ExcepcionMatrizAdyacenciaIncoherente, ExcepcionNodoInexistente, ExcepcionGrafoCiclico;

    /**
     * Establece una relación direccional entre dos nodos.
     *
     * @param nombrePadre nombre del nodo padre de la relación a crear.
     * @param nombreHijo nombre del nodo hijo de la relación a crear.
     * @throws ExcepcionDPCExistente Si existe una tabla de probabilidad
     * condicionada que quedaría inutilizada de crearse la relación.
     * @throws ExcepcionNodoInexistente Si no se localiza alguno de los nodos
     * solicitados por su nombre.
     */
    public void addRelacion(final String nombrePadre, final String nombreHijo) throws ExcepcionDPCExistente, ExcepcionNodoInexistente;

    /**
     *
     * @param idPadre id del nodo padre de la relación a crear.
     * @param idHijo id del nodo hijo de la relación a crear.
     * @throws ExcepcionDPCExistente Si existe una tabla de probabilidad
     * condicionada que quedaría inutilizada de crearse la relación.
     * @throws ExcepcionNodoInexistente Si no se localiza alguno de los nodos
     * solicitados por su id.
     */
    public void addRelacion(final int idPadre, final int idHijo) throws ExcepcionDPCExistente, ExcepcionNodoInexistente;

    /**
     *
     * @param nombrePadre nombre del nodo padre de la relación a eliminar.
     * @param nombreHijo nombre del nodo hijo de la relación a eliminar.
     * @throws ExcepcionDPCExistente Si existe una tabla de probabilidad
     * condicionada que quedaría inutilizada de eliminarse la relación.
     * @throws ExcepcionNodoInexistente Si no se localiza alguno de los nodos
     * solicitados por su nombre.
     */
    public void deleteRelacion(final String nombrePadre, final String nombreHijo) throws ExcepcionNodoInexistente, ExcepcionDPCExistente;

    /**
     *
     * @param idPadre id del nodo padre de la relación a eliminar.
     * @param idHijo id del nodo hijo de la relación a eliminar.
     * @throws ExcepcionDPCExistente Si existe una tabla de probabilidad
     * condicionada que quedaría inutilizada de eliminarse la relación.
     * @throws ExcepcionNodoInexistente Si no se localiza alguno de los nodos
     * solicitados por su id.
     */
    public void deleteRelacion(final int idPadre, final int idHijo) throws ExcepcionNodoInexistente, ExcepcionDPCExistente;

    /**
     * Define los estados de un nodo concreto.
     *
     * @param nombreNodo nombre del nodo cuyos estados se van a fijar.
     * @param estados lista de nombres de los estados que va a poseer el nodo.
     * @throws ExcepcionDPCExistente Si existe una tabla de probabilidad
     * condicionada que quedaria inutilizada de modificarse los estados del
     * nodo.
     * @throws ExcepcionEstadoDuplicado Si hay mas de un estado con el mismo
     * nombre en la lista introducida.
     * @throws ExcepcionNodoInexistente Si no se localiza el nodo por su nombre
     * en la red.
     */
    public void setEstadosNodo(final String nombreNodo, final List<String> estados) throws ExcepcionDPCExistente, ExcepcionEstadoDuplicado, ExcepcionNodoInexistente;

    /**
     * Define los estados de un nodo concreto.
     *
     * @param idNodo id del nodo cuyos estados se van a fijar.
     * @param estados lista de nombres de los estados que va a poseer el nodo.
     * @throws ExcepcionDPCExistente Si existe una tabla de probabilidad
     * condicionada que quedaria inutilizada de modificarse los estados del
     * nodo.
     * @throws ExcepcionEstadoDuplicado Si hay mas de un estado con el mismo
     * nombre en la lista introducida.
     * @throws ExcepcionNodoInexistente Si no se localiza el nodo por su id en
     * la red.
     */
    public void setEstadosNodo(final int idNodo, final List<String> estados) throws ExcepcionDPCExistente, ExcepcionEstadoDuplicado, ExcepcionNodoInexistente;

    /**
     * Añade un estado a un nodo concreto.
     *
     * @param nombreNodo nombre del nodo al que se le va a añadir el nuevo
     * estado.
     * @param estado nombre del nuevo estado.
     * @throws ExcepcionDPCExistente Si existe una tabla de probabilidad
     * condicionada que quedaría inutilizada de añadir el nuevo estado.
     * @throws ExcepcionEstadoDuplicado Si ya existe en el nodo un estado con
     * ese nombre.
     * @throws ExcepcionNodoInexistente Si no se localiza un nodo con ese nombre
     * en la red.
     */
    public void addEstadoNodo(final String nombreNodo, final String estado) throws ExcepcionDPCExistente, ExcepcionEstadoDuplicado, ExcepcionNodoInexistente;

    /**
     *
     * @param idNodo id del nodo al que se le va a añadir el nuevo estado.
     * @param estado nombre del nuevo estado.
     * @throws ExcepcionDPCExistente Si existe una tabla de probabilidad
     * condicionada que quedaría inutilizada de añadir el nuevo estado.
     * @throws ExcepcionEstadoDuplicado Si ya existe en el nodo un estado con
     * ese nombre.
     * @throws ExcepcionNodoInexistente Si no se localiza un nodo con ese id en
     * la red.
     */
    public void addEstadoNodo(final int idNodo, final String estado) throws ExcepcionDPCExistente, ExcepcionEstadoDuplicado, ExcepcionNodoInexistente;

    /**
     * Elimina un estado concreto de un Nodo.
     *
     * @param nombreNodo nombre del nodo que posee el estado a eliminar.
     * @param estado nombre del estado a eliminar.
     * @throws ExcepcionDPCExistente Si existe tabla de probabilidad
     * condicionada que quedaría inutilizada de eliminar el estado.
     * @throws ExcepcionEstadoInexistente Si el nodo no posee un estado con ese
     * nombre.
     * @throws ExcepcionNodoInexistente Si no se localiza un nodo con ese nombre
     * en la red.
     */
    public void deleteEstadoNodo(final String nombreNodo, final String estado) throws ExcepcionDPCExistente, ExcepcionEstadoInexistente, ExcepcionNodoInexistente;

    /**
     * Elimina un estado concreto de un Nodo.
     *
     * @param idNodo id del nodo que posee el estado a eliminar.
     * @param estado nombre del estado a eliminar.
     * @throws ExcepcionDPCExistente Si existe tabla de probabilidad
     * condicionada que quedaría inutilizada de eliminar el estado.
     * @throws ExcepcionEstadoInexistente Si el nodo no posee un estado con ese
     * nombre.
     * @throws ExcepcionNodoInexistente Si no se localiza un nodo con ese id en
     * la red.
     */
    public void deleteEstadoNodo(final int idNodo, final String estado) throws ExcepcionDPCExistente, ExcepcionEstadoInexistente, ExcepcionNodoInexistente;

    /**
     * Fija un nodo a un estado concreto.
     *
     * @param nombreNodo nombre del nodo al que se le va a fijar un estado.
     * @param estado nombre del estado a fijar.
     * @throws ExcepcionEstadoInexistente Si el nodo no posee un estado con ese
     * nombre.
     * @throws ExcepcionNodoInexistente Si no se localiza un nodo con ese nombre
     * en la red.
     */
    public void fijaEstadoNodo(final String nombreNodo, final String estado) throws ExcepcionNodoInexistente, ExcepcionEstadoInexistente;

    /**
     * Fija un nodo a un estado concreto.
     *
     * @param idNodo id del nodo al que se le va a fijar un estado.
     * @param estado nombre del estado a fijar.
     * @throws ExcepcionEstadoInexistente Si el nodo no posee un estado con ese
     * nombre.
     * @throws ExcepcionNodoInexistente Si no se localiza un nodo con ese nombre
     * en la red.
     */
    public void fijaEstadoNodo(final int idNodo, final String estado) throws ExcepcionNodoInexistente, ExcepcionEstadoInexistente;

    /**
     * Libera a un nodo de un estado fijado.
     *
     * @param nombreNodo nombre del nodo al que se le va a eliminar cualquier
     * evidencia.
     * @throws ExcepcionNodoInexistente Si no se localiza un nodo con ese nombre
     * en la red.
     */
    public void clearEstadoNodo(final String nombreNodo) throws ExcepcionNodoInexistente;

    /**
     * Libera a un nodo de un estado fijado.
     *
     * @param idNodo id del nodo al que se le va a eliminar cualquier evidencia.
     * @throws ExcepcionNodoInexistente Si no se localiza un nodo con ese nombre
     * en la red.
     */
    public void clearEstadoNodo(final int idNodo) throws ExcepcionNodoInexistente;

    /**
     * Libera a todos los nodos de estados fijados.
     */
    public void clearAllEstadosNodos();

    /**
     * Fija la tabla de distribucion de probabilidad condicionada de un nodo a
     * la matriz recibida.
     *
     * @param nombreNodo nombre del nodo al que se le va a asignar la tabla.
     * @param dPC matriz que contiene la tabla de distribucion de probabilidad
     * condidionada.
     * @throws ExcepcionDPCIncoherente Si la matriz no es correcta o coherente
     * con la red y el nodo.
     * @throws ExcepcionNodoInexistente Si no se localiza un nodo con ese nombre
     * en la red.
     */
    public void setTablaDPCNodo(final String nombreNodo, final List<ArrayList<Double>> dPC) throws ExcepcionDPCIncoherente, ExcepcionNodoInexistente;

    /**
     * Fija la tabla de distribucion de probabilidad condicionada de un nodo a
     * la matriz recibida.
     *
     * @param idNodo nombre del nodo al que se le va a asignar la tabla.
     * @param dPC matriz que contiene la tabla de distribucion de probabilidad
     * condidionada.
     * @throws ExcepcionDPCIncoherente Si la matriz no es correcta o coherente
     * con la red y el nodo.
     * @throws ExcepcionNodoInexistente Si no se localiza un nodo con ese id en
     * la red.
     */
    public void setTablaDPCNodo(final int idNodo, final List<ArrayList<Double>> dPC) throws ExcepcionDPCIncoherente, ExcepcionNodoInexistente;

    /**
     * Elimina la tabla de distribución de probabilidad condicionada de un Nodo.
     *
     * @param nombreNodo nombre del nodo cuya tabla se va a eliminar.
     * @throws ExcepcionNodoInexistente Si no se localiza un nodo con ese nombre
     * en la red.
     */
    public void deleteTablaDPCNodo(final String nombreNodo) throws ExcepcionNodoInexistente;

    /**
     * Elimina la tabla de distribución de probabilidad condicionada de un Nodo.
     *
     * @param idNodo id del nodo cuya tabla se va a eliminar.
     * @throws ExcepcionNodoInexistente Si no se localiza un nodo con ese id en
     * la red.
     */
    public void deleteTablaDPCNodo(final int idNodo) throws ExcepcionNodoInexistente;

    // 2. Funciones de "Ver datos"
    /**
     * Devuelve el número de nodos de la red.
     *
     * @return numero de nodos de la red.
     */
    public int numNodos();

    /**
     * Devuelve el Nodo solicitado.
     *
     * @param nombreNodo nombre del nodo solicitado.
     * @return nodo solicitado.
     * @throws ExcepcionNodoInexistente Si no se localiza un nodo con ese nombre
     * en la red.
     */
    public Nodo getNodo(final String nombreNodo) throws ExcepcionNodoInexistente;

    /**
     * Devuelve el Nodo solicitado.
     *
     * @param idNodo id del nodo solicitado.
     * @return nodo solicitado.
     * @throws ExcepcionNodoInexistente Si no se localiza un nodo con ese id en
     * la red.
     */
    public Nodo getNodo(final int idNodo) throws ExcepcionNodoInexistente;

    /**
     * Indica si existe la relación direccional por la que se pregunta.
     *
     * @param nombrePadre nombre del nodo padre de la relacion.
     * @param nombreHijo nombre del nodo hijo de la relacion.
     * @return true si existe relación padre -> hijo.
     * @throws ExcepcionNodoInexistente Si no se localiza alguno de los nodos
     * por esos nombres en la red.
     */
    public boolean hayRelacion(final String nombrePadre, final String nombreHijo) throws ExcepcionNodoInexistente;

    /**
     * Indica si existe la relación direccional por la que se pregunta.
     *
     * @param idPadre id del nodo padre de la relacion.
     * @param idHijo id del nodo hijo de la relacion.
     * @return true si existe relación padre -> hijo, false si no.
     * @throws ExcepcionNodoInexistente Si no se localiza alguno de los nodos
     * por esos id en la red.
     */
    public boolean hayRelacion(final int idPadre, final int idHijo) throws ExcepcionNodoInexistente;

    /**
     * Comprueba que no existan bucles. Utiliza la función ordenaGrafo
     *
     * @param grafo Grado a encontrar bucles.
     * @throws ExcepcionGrafoCiclico Si existe al menos un bucle.
     */
    public void compruebaAciclico(final List<ArrayList<Integer>> grafo) throws ExcepcionGrafoCiclico;

    /**
     * Comprueba que no existan bucles en el grafo actual. Utiliza la funcion
     * ordenaGrafo
     *
     * @throws ExcepcionGrafoCiclico Si existe al menos un bucle.
     */
    public void compruebaAciclico() throws ExcepcionGrafoCiclico;

    /**
     * Devuelve una ordenación topológica del grafo solicitado. Se basa en el
     * siguiente algoritmo de ordenación topológica: L ← Empty list where we put
     * the sorted elements Q ← Set of all nodes with no incoming edges while Q
     * is non-empty do ....remove a node n from Q ....insert n into L ....for
     * each node m with an edge e from n to m do ........remove edge e from the
     * graph ........if m has no other incoming edges then ............insert m
     * into Q if graph has edges then ....output error message (graph has a
     * cycle) else ....output message (proposed topologically sorted order: L)
     *
     * @param grafo Grado a encontrar bucles.
     * @return lista de elementos ordenados topológicamente
     * @throws ExcepcionGrafoCiclico Si existe al menos un bucle.
     */
    public List<Integer> ordenaGrafo(final List<ArrayList<Integer>> grafo) throws ExcepcionGrafoCiclico;

    /**
     * Devuelve una lista de nodos con un orden topológico de la red actual.
     * Utiliza la funcion ordenaGrafo
     *
     * @return lista de nodos ordenados topológicamente
     * @throws ExcepcionGrafoCiclico Si existe al menos un bucle.
     * @throws ExcepcionNodoInexistente Si al ir recorriendo los nodos en las
     * consultas se encuentra uno que no existe.
     */
    public List<Nodo> ordenTopologico() throws ExcepcionGrafoCiclico, ExcepcionNodoInexistente;

    /**
     * Devuelve la probabilidad de una combinación de variables de acuerdo a las
     * evidencias fijadas en la red.
     *
     * @param nombreNodosSolicitados
     * @return factor con la probabilidad solicitada normalizada.
     * @throws ExcepcionDPCInexistente Si no existe alguna tabla de distribución
     * de probabilidad al operar.
     * @throws ExcepcionGrafoCiclico Si el grafo contiene bucles.
     * @throws ExcepcionNodoInexistente Si no existe un nodo solicitado.
     * @throws ExcepcionFactor Si se da algún problema al operar con factores.
     * @throws ExcepcionEvidenciaInexistente Si se indica alguna evidencia no
     * existentes.
     */
    public Factor probabilidad(final List<String> nombreNodosSolicitados) throws ExcepcionDPCInexistente, ExcepcionGrafoCiclico, ExcepcionNodoInexistente, ExcepcionFactor, ExcepcionEvidenciaInexistente;
}