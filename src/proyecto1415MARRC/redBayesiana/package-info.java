/**
 * <p>
 * Contiene todas los elementos y estructuras de datos necesarios para modelar una red bayesiana: Nodo,
 * Factor, RedBayesiana, etc.
 * </p>
 * Además incluye las excepciones propias del modelo.
 *
 *
 * @author marrc1415
 * @since 1.0-FINAL
 * @version 1.0-FINAL
 */
package proyecto1415MARRC.redBayesiana;