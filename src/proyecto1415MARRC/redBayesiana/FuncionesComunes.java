/*
 * Desarrollo de una pequeña herramienta que permita representar y razonar
 * (realizando inferencias) sobre una Red Bayesiana de una forma sencilla.
 * Métodos avanzados de razonamiento y representación del conocimiento, Máster
 * en Ingeniería Informática, curso 2014-2015, Universidad de Valladolid.
 * Autores: Borja Cerezo, Cristian Tejedor y Daniel Roda.
 */
package proyecto1415MARRC.redBayesiana;

import java.util.List;

/**
 * Clase que modela funciones auxiliares comunes.
 *
 * @author marrc1415
 * @since 0.2-BETA
 * @version 1.0-FINAL
 */
public final class FuncionesComunes {

    /**
     * Calcula los estados de los nodos de un factor para la siguiente posición
     * de la lista de factores.
     *
     * @param elementosActuales Lista de combinación de elementos en el momento
     * actual.
     * @param numValoresElementos Lista que almacena el número de valores
     * posibles de cada elemento, y por tanto el valor de desbordamiento de cada
     * posicion de la lista elementosActuales.
     * @return true si se obtiene el siguiente estado. false si ha habido
     * desbordamiento global y se han recorrido todas las combinaciones posibles
     * de estados de los padres del nodo.
     */
    public static boolean siguienteCombinacion(final List<Integer> elementosActuales,
            final List<Integer> numValoresElementos) {
        return siguienteCombinacion(elementosActuales, numValoresElementos, (elementosActuales.size() - 1));
    }

    /**
     * Calcula los estados de los nodos de un factor para la siguiente posición
     * de la lista de factores.
     *
     * @param elementosActuales Lista de combinación de elementos en el momento
     * actual.
     * @param numValoresElementos Lista que almacena el número de valores
     * posibles de cada elemento, y por tanto el valor de desbordamiento de cada
     * posicion de la lista elementosActuales.
     * @param posicion posicion del elemento a incrementar en la iteracion
     * actual.
     * @return true si se obtiene el siguiente estado. false si ha habido
     * desbordamiento global y se han recorrido todas las combinaciones posibles
     * de estados de los padres del nodo.
     */
    public static boolean siguienteCombinacion(final List<Integer> elementosActuales,
            final List<Integer> numValoresElementos, int posicion) {
        final int valorEstadoPadreObservado = elementosActuales.get(posicion);
        if (valorEstadoPadreObservado + 1 == ((numValoresElementos.get(posicion)))) {
            if (posicion == 0) {
                return false;
            }
            elementosActuales.set(posicion, 0);
            posicion--;
            return siguienteCombinacion(elementosActuales, numValoresElementos, posicion);
        }
        elementosActuales.set(posicion, valorEstadoPadreObservado + 1);
        return true;
    }

    /**
     * Calcula los estados de los nodos de un factor para la siguiente posición
     * de la lista de factores.
     *
     * @param elementosActuales Lista de combinación de elementos en el momento
     * actual.
     * @param numValoresElementos Lista que almacena el número de valores
     * posibles de cada elemento, y por tanto el valor de desbordamiento de cada
     * posicion de la lista elementosActuales.
     * @param pivotes Lista los elementos que son constantes y no varían en las
     * combinaciones.
     * @return true si se obtiene el siguiente estado. false si ha habido
     * desbordamiento global y se han recorrido todas las combinaciones posibles
     * de estados de los padres del nodo.
     */
    public static boolean siguienteCombinacion(final List<Integer> elementosActuales,
            final List<Integer> numValoresElementos, final List<Integer> pivotes) {
        return siguienteCombinacion(elementosActuales, numValoresElementos, pivotes, (elementosActuales.size() - 1));
    }

    /**
     * Calcula los estados de los nodos de un factor para la siguiente posición
     * de la lista de factores.
     *
     * @param elementosActuales Lista de combinación de elementos en el momento
     * actual.
     * @param numValoresElementos Lista que almacena el número de valores
     * posibles de cada elemento, y por tanto el valor de desbordamiento de cada
     * posicion de la lista elementosActuales.
     * @param pivotes Lista los elementos que son constantes y no varían en las
     * combinaciones.
     * @param posicion posicion del elemento a incrementar en la iteracion
     * actual.
     * @return true si se obtiene el siguiente estado. false si ha habido
     * desbordamiento global y se han recorrido todas las combinaciones posibles
     * de estados de los padres del nodo.
     */
    public static boolean siguienteCombinacion(final List<Integer> elementosActuales,
            final List<Integer> numValoresElementos, final List<Integer> pivotes, int posicion) {
        final int valorEstadoPadreObservado = elementosActuales.get(posicion);
        if (pivotes.contains(posicion)) {
            posicion--;
            return (posicion < 0 ? false : siguienteCombinacion(elementosActuales, numValoresElementos, pivotes, posicion));
        }
        if (valorEstadoPadreObservado + 1 == ((numValoresElementos.get(posicion)))) {
            if (posicion == 0) {
                return false;
            }
            elementosActuales.set(posicion, 0);
            posicion--;
            return siguienteCombinacion(elementosActuales, numValoresElementos, pivotes, posicion);

        }
        elementosActuales.set(posicion, valorEstadoPadreObservado + 1);
        return true;
    }
}