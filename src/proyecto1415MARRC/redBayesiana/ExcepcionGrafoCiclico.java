/*
 * Desarrollo de una pequeña herramienta que permita representar y razonar
 * (realizando inferencias) sobre una Red Bayesiana de una forma sencilla.
 * Métodos avanzados de razonamiento y representación del conocimiento, Máster
 * en Ingeniería Informática, curso 2014-2015, Universidad de Valladolid.
 * Autores: Borja Cerezo, Cristian Tejedor y Daniel Roda.
 */
package proyecto1415MARRC.redBayesiana;

/**
 * Excepción ocurrida cuando la red introducida contiene bucles.
 *
 * @author marrc1415
 * @since 0.2-BETA
 * @version 1.0-FINAL
 */
public final class ExcepcionGrafoCiclico extends Exception {

    /**
     * Mensaje por defecto de la excepción.
     */
    private final static String MENSAJE = "La red introducida debe ser acíclica y no lo es.";

    /**
     * Constructor por defecto que muestra el mensaje explicatorio.
     */
    public ExcepcionGrafoCiclico() {
        super(MENSAJE);
    }

    /**
     * Muestra un mensaje de error extra al mensaje explicatorio por defecto.
     *
     * @param msg Message error.
     */
    public ExcepcionGrafoCiclico(final String msg) {
        super(MENSAJE + '\n' + msg);
    }
}