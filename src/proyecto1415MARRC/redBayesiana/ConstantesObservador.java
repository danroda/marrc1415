/*
 * Desarrollo de una peque?a herramienta que permita representar y razonar
 * (realizando inferencias) sobre una Red Bayesiana de una forma sencilla.
 * "Métodos avanzados de razonamiento y representaci?n del conocimiento", Máster
 * en Ingeniería Informática, curso 2014-2015, Universidad de Valladolid.
 * Autores: Borja Cerezo, Cristian Tejedor y Daniel Roda.
 */
package proyecto1415MARRC.redBayesiana;

/**
 * Clase que almacena las constantes usadas por el observador de nodos.
 *
 * @author marrc1415
 * @since 0.2-BETA
 * @version 1.0-FINAL
 */
public final class ConstantesObservador {

    /**
     * Número que indica a un observador que la actualización es de relación.
     */
    public static final int ACTUALIZA_RELACION = 0;
    /**
     * Número que indica a un observador que la actualización es de evidencia.
     */
    public static final int ACTUALIZA_EVIDENCIA = 1;
    /**
     * Número que indica que no hay un estado fijado en un Nodo.
     */
    public static final int NO_EVIDENCIA = -1;
}