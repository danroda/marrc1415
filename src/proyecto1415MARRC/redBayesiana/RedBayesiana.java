/*
 * Desarrollo de una pequeña herramienta que permita representar y razonar
 * (realizando inferencias) sobre una Red Bayesiana de una forma sencilla.
 * Métodos avanzados de razonamiento y representación del conocimiento, Máster
 * en Ingeniería Informática, curso 2014-2015, Universidad de Valladolid.
 * Autores: Borja Cerezo, Cristian Tejedor y Daniel Roda.
 */
package proyecto1415MARRC.redBayesiana;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import proyecto1415MARRC.logger.Log;

/**
 * Implementación concreta de una red bayesiana.
 *
 * @author marrc1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public final class RedBayesiana implements IRedBayesiana, Observer {

    /**
     * Lista de nodos que componen la red.
     */
    private List<Nodo> nodos;
    /**
     * Lista de adyacencia de relaciones de los nodos en la red.
     */
    private List<ArrayList<Integer>> adyacencia;
    /**
     * Lista de adyacencia de relaciones de los nodos en la red.
     */
    private List<Integer> evidencia;

    /**
     * Constructor por defecto.
     */
    public RedBayesiana() {
        adyacencia = new ArrayList<ArrayList<Integer>>();
        nodos = new ArrayList<Nodo>();
        evidencia = new ArrayList<Integer>();
    }

    /*Dar valores*/
    @Override
    public void addNodo(final String nombreNodo) throws ExcepcionNodoDuplicado {
        for (final Nodo nodoObservado : this.nodos) {
            if (nodoObservado.getNombre().equals(nombreNodo)) {
                throw new ExcepcionNodoDuplicado("Ya existe un nodo con el nombre " + nombreNodo + " .");
            }
        }
        int idNodo = this.nodos.size();
        final Nodo nodoObservable = new Nodo(nombreNodo, idNodo);
        this.nodos.add(nodoObservable);
        nodoObservable.addObserver(this);
        for (final ArrayList<Integer> fila : this.adyacencia) {
            fila.add(0);
        }
        final ArrayList<Integer> nuevaFila = new ArrayList<Integer>();
        int numColumnas = this.nodos.size();
        for (int i = 0; i < numColumnas; i++) {
            nuevaFila.add(0);
        }
        this.adyacencia.add(nuevaFila);
        this.evidencia.add(ConstantesObservador.NO_EVIDENCIA);
    }

    @Override
    public void deleteNodo(final String nombreNodo) throws ExcepcionNodoInexistente, ExcepcionDPCExistente {
        deleteNodo(getNodo(nombreNodo));
    }

    @Override
    public void deleteNodo(final int idNodo) throws ExcepcionNodoInexistente, ExcepcionDPCExistente {
        deleteNodo(getNodo(idNodo));
    }

    /**
     * Elimina un nodo de la red.
     *
     * @param nodo Nodo a eliminar.
     */
    private void deleteNodo(final Nodo nodo) throws ExcepcionNodoInexistente, ExcepcionDPCExistente {
        if (nodo.tieneDPC()) {
            throw new ExcepcionDPCExistente("No se puede eliminar el nodo " + nodo.getNombre() + " porque posee tabla DPC.");
        }
        if (nodo.hijoTieneDPC()) {
            throw new ExcepcionDPCExistente("No se puede eliminar el nodo " + nodo.getNombre() + " porque posee algún hijo con tabla DPC.");
        }

        int numNodosDesconectar = nodo.getNumPadres();
        for (int i = 0; i < numNodosDesconectar; i++) {
            nodo.deletePadre(i);
        }
        numNodosDesconectar = nodo.getNumHijos();
        for (int i = 0; i < numNodosDesconectar; i++) {
            nodo.deleteHijo(i);
        }
        final int posicion = nodo.getId();
        adyacencia.remove(posicion);
        for (final ArrayList<Integer> fila : this.adyacencia) {
            fila.remove(posicion);
        }
        nodos.remove(posicion).deleteObserver(this);
        final int numNodos = numNodos();
        for (int i = posicion; i < numNodos; i++) {
            nodos.get(i).setId(this);
        }

    }

    @Override
    public void setAdyacencia(final List<ArrayList<Integer>> adyacencia) throws ExcepcionDPCExistente, ExcepcionMatrizAdyacenciaIncoherente, ExcepcionNodoInexistente, ExcepcionGrafoCiclico {
        for (final Nodo nodo : this.nodos) {
            if (nodo.tieneDPC()) {
                throw new ExcepcionDPCExistente("No puede fijarse una nueva matriz de adyacencia porque el nodo " + nodo.getNombre() + "tiene tabla DPC.");
            }
        }
        final int numNodos = nodos.size();
        if (adyacencia.size() != numNodos) {
            throw new ExcepcionMatrizAdyacenciaIncoherente("La matriz de adyacencia debería tener " + numNodos + " filas, pero tiene " + adyacencia.size() + ".");
        }
        for (final ArrayList<Integer> fila : adyacencia) {
            if (fila.size() != numNodos) {
                throw new ExcepcionMatrizAdyacenciaIncoherente("La matriz de adyacencia debería tener " + numNodos + " columnas en la fila " + adyacencia.indexOf(fila) + 1 + ", pero tiene " + fila.size() + ".");
            }
        }
        compruebaAciclico(adyacencia);
        for (final Nodo nodo : this.nodos) {
            for (final Nodo hijo : nodo.getHijos()) {
                nodo.deleteHijo(hijo.getId());
            }
        }
        ArrayList<Integer> fila;
        for (int idPadre = 0; idPadre < numNodos; idPadre++) {
            fila = adyacencia.get(idPadre);
            for (int idHijo = 0; idHijo < numNodos; idHijo++) {
                if (fila.get(idHijo) == 0) {
                    adyacencia.get(idPadre).set(idHijo, 0);
                } else {
                    addRelacion(idPadre, idHijo);
                }
            }
        }
    }

    @Override
    public void addRelacion(final String nombrePadre, final String nombreHijo) throws ExcepcionDPCExistente, ExcepcionNodoInexistente {
        getNodo(nombreHijo).addPadre(getNodo(nombrePadre));
    }

    @Override
    public void addRelacion(final int idPadre, final int idHijo) throws ExcepcionDPCExistente, ExcepcionNodoInexistente {
        getNodo(idHijo).addPadre(getNodo(idPadre));
    }

    @Override
    public void deleteRelacion(final String nombrePadre, final String nombreHijo) throws ExcepcionDPCExistente, ExcepcionNodoInexistente {
        getNodo(nombreHijo).deletePadre(nombrePadre);
    }

    @Override
    public void deleteRelacion(final int idPadre, final int idHijo) throws ExcepcionDPCExistente, ExcepcionNodoInexistente {
        getNodo(idHijo).deletePadre(idPadre);
    }

    @Override
    public void setEstadosNodo(final String nombreNodo, final List<String> estados) throws ExcepcionDPCExistente, ExcepcionEstadoDuplicado, ExcepcionNodoInexistente {
        getNodo(nombreNodo).setEstados(estados);
    }

    @Override
    public void setEstadosNodo(final int idNodo, final List<String> estados) throws ExcepcionDPCExistente, ExcepcionEstadoDuplicado, ExcepcionNodoInexistente {
        getNodo(idNodo).setEstados(estados);
    }

    @Override
    public void addEstadoNodo(final String nombreNodo, final String estado) throws ExcepcionDPCExistente, ExcepcionEstadoDuplicado, ExcepcionNodoInexistente {
        getNodo(nombreNodo).addEstado(estado);
    }

    @Override
    public void addEstadoNodo(final int idNodo, final String estado) throws ExcepcionDPCExistente, ExcepcionEstadoDuplicado, ExcepcionNodoInexistente {
        getNodo(idNodo).addEstado(estado);
    }

    @Override
    public void deleteEstadoNodo(final String nombreNodo, final String estado) throws ExcepcionDPCExistente, ExcepcionEstadoInexistente, ExcepcionNodoInexistente {
        getNodo(nombreNodo).deleteEstado(estado);
    }

    @Override
    public void deleteEstadoNodo(final int idNodo, final String estado) throws ExcepcionDPCExistente, ExcepcionEstadoInexistente, ExcepcionNodoInexistente {
        getNodo(idNodo).deleteEstado(estado);
    }

    @Override
    public void fijaEstadoNodo(String nombreNodo, String estado) throws ExcepcionNodoInexistente, ExcepcionEstadoInexistente {
        getNodo(nombreNodo).setEstadoFijado(estado);
    }

    @Override
    public void fijaEstadoNodo(int idNodo, String estado) throws ExcepcionNodoInexistente, ExcepcionEstadoInexistente {
        getNodo(idNodo).setEstadoFijado(estado);
    }

    @Override
    public void clearEstadoNodo(String nombreNodo) throws ExcepcionNodoInexistente {
        getNodo(nombreNodo).clearEstadoFijado();
    }

    @Override
    public void clearEstadoNodo(int idNodo) throws ExcepcionNodoInexistente {
        getNodo(idNodo).clearEstadoFijado();
    }

    @Override
    public void clearAllEstadosNodos() {
        for (final Nodo nodo : nodos) {
            if (nodo.tieneEstadoFijado()) {
                nodo.clearEstadoFijado();
            }
        }
    }

    @Override
    public void setTablaDPCNodo(final String nombreNodo, final List<ArrayList<Double>> dPC) throws ExcepcionDPCIncoherente, ExcepcionNodoInexistente {
        getNodo(nombreNodo).setTablaDPC(dPC);
    }

    @Override
    public void setTablaDPCNodo(final int idNodo, final List<ArrayList<Double>> dPC) throws ExcepcionDPCIncoherente, ExcepcionNodoInexistente {
        getNodo(idNodo).setTablaDPC(dPC);
    }

    @Override
    public void deleteTablaDPCNodo(final String nombreNodo) throws ExcepcionNodoInexistente {
        getNodo(nombreNodo).deleteTablaDPC();
    }

    @Override
    public void deleteTablaDPCNodo(final int idNodo) throws ExcepcionNodoInexistente {
        getNodo(idNodo).deleteTablaDPC();
    }

    /*Ver datos*/
    @Override
    public int numNodos() {
        return nodos.size();
    }

    @Override
    public Nodo getNodo(final int idNodo) throws ExcepcionNodoInexistente {
        for (final Nodo nodoObservado : this.nodos) {
            if (nodoObservado.getId() == idNodo) {
                return nodoObservado;
            }
        }
        throw new ExcepcionNodoInexistente("No se encuentra nodo con id: " + idNodo + ".");
    }

    @Override
    public Nodo getNodo(final String nombreNodo) throws ExcepcionNodoInexistente {
        for (final Nodo nodoObservado : this.nodos) {
            if (nodoObservado.getNombre().equals(nombreNodo)) {
                return nodoObservado;
            }
        }
        throw new ExcepcionNodoInexistente("No se encuentra nodo con nombre: " + nombreNodo + ".");
    }

    @Override
    public boolean hayRelacion(final String nombrePadre, final String nombreHijo) throws ExcepcionNodoInexistente {
        return (adyacencia.get(getNodo(nombrePadre).getId()).get(getNodo(nombrePadre).getId()) != 0);
    }

    @Override
    public boolean hayRelacion(final int idPadre, final int idHijo) throws ExcepcionNodoInexistente {
        return (adyacencia.get(idPadre).get(idHijo) != 0);
    }

    @Override
    /**
     *
     * Patrón observador - Matriz Adyacencia. Actualiza el estado del nodo padre
     * según el estado de su nodo hijo.
     *
     * @param Observable Objeto observado.
     * @param arg[0] padre - ID del nodo padre.
     * @param arg[1] hijo - ID del nodo hijo.
     * @param arg[2] estado - Estado del nodo.
     */
    public void update(final Observable o, final Object arg) {
        try {
            final Integer[] peticion = (Integer[]) arg;
            switch (peticion[0]) {
                case ConstantesObservador.ACTUALIZA_RELACION:
                    int padreId = peticion[1];
                    int hijoId = peticion[2];
                    int estado = peticion[3];
                    this.adyacencia.get(padreId).set(hijoId, estado);
                    break;
                case ConstantesObservador.ACTUALIZA_EVIDENCIA:
                    int nodoId = peticion[1];
                    int estadoId = peticion[2];
                    this.evidencia.set(nodoId, estadoId);
                    break;
            }

        } catch (final Exception ex) {
            // No se actualiza nada.
        }
    }

    @Override
    public List<Integer> ordenaGrafo(final List<ArrayList<Integer>> grafo) throws ExcepcionGrafoCiclico {
        final int numNodos = grafo.size();
        // Grafo auxiliar
        final List<ArrayList<Integer>> auxGrafo = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < numNodos; i++) {
            auxGrafo.add(new ArrayList<Integer>());
        }
        //Empty list where we put the sorted elements
        final List<Integer> L = new ArrayList<Integer>();
        //  Set of all nodes with no incoming edges
        final List<Integer> Q = new ArrayList<Integer>();
        // Indica si no es un hijo de algún nodo.
        boolean noHijo;
        // Recordatorio: filas=padres y columnas=hijos
        // Se recorre por columnas, buscando 1's => es hijo de algún padre y se descarta
        for (int columna = 0; columna < numNodos; columna++) {
            noHijo = true; // No tiene padres por defecto.
            for (int fila = 0; fila < numNodos; fila++) {
                auxGrafo.get(fila).add(grafo.get(fila).get(columna));
                // Tiene padre => es hijo, ya no vale
                if (grafo.get(fila).get(columna) != 0) {
                    noHijo = false;
                }
            }
            if (noHijo) {
                Q.add(columna);
            }
        }
        int nodoPadre;
        while (Q.size() != 0) {
            nodoPadre = Q.get(0);
            L.add(nodoPadre);
            Q.remove(0);
            for (int nodoHijo = 0; nodoHijo < numNodos; nodoHijo++) {
                if (auxGrafo.get(nodoPadre).get(nodoHijo) != 0) {
                    auxGrafo.get(nodoPadre).set(nodoHijo, 0);
                    noHijo = true;
                    for (int posiblePadre = 0; posiblePadre < numNodos; posiblePadre++) {
                        if (auxGrafo.get(posiblePadre).get(nodoHijo) != 0) {
                            noHijo = false;
                        }
                    }
                    if (noHijo) {
                        Q.add(nodoHijo);
                    }
                }
            }
        }
        for (int fila = 0; fila < numNodos; fila++) {
            for (int columna = 0; columna < numNodos; columna++) {
                if (auxGrafo.get(fila).get(columna) != 0) {
                    throw new ExcepcionGrafoCiclico();
                }
            }
        }
        return L;
    }

    @Override
    public void compruebaAciclico() throws ExcepcionGrafoCiclico {
        compruebaAciclico(this.adyacencia);
    }

    @Override
    public void compruebaAciclico(final List<ArrayList<Integer>> grafo) throws ExcepcionGrafoCiclico {
        List<Integer> aux = ordenaGrafo(grafo);
    }

    @Override
    public List<Nodo> ordenTopologico() throws ExcepcionGrafoCiclico, ExcepcionNodoInexistente {
        final List<Integer> idNodos = ordenaGrafo(this.adyacencia);
        final List<Nodo> nodosOrdenados = new ArrayList<Nodo>();
        for (int idNodo : idNodos) {
            nodosOrdenados.add(this.getNodo(idNodo));
        }
        return nodosOrdenados;
    }

    /**
     * Calcula la probabilidad de un factor.
     *
     * @param nombreNodosSolicitados
     * @return
     * @throws ExcepcionDPCInexistente
     * @throws ExcepcionGrafoCiclico
     * @throws ExcepcionNodoInexistente
     * @throws ExcepcionFactor
     * @throws ExcepcionEvidenciaInexistente
     */
    public Factor probabilidad(final List<String> nombreNodosSolicitados) throws ExcepcionDPCInexistente, ExcepcionGrafoCiclico, ExcepcionNodoInexistente, ExcepcionFactor, ExcepcionEvidenciaInexistente {
        final int numNodos = this.nodos.size();
        int solicitudesCorrectas = 0;
        final List<Integer> ordenIdNodos = ordenaGrafo(adyacencia);
        final List<String> nodosAEliminarOrdenados = new ArrayList<String>();
        final List<Integer> evidencias = new ArrayList<Integer>();
        String nombreNodo;
        for (final int idNodo : ordenIdNodos) {
            nombreNodo = getNodo(idNodo).getNombre();
            if (!nombreNodosSolicitados.contains(nombreNodo)) {
                nodosAEliminarOrdenados.add(nombreNodo);
            } else {
                solicitudesCorrectas++;
                if (evidencia.get(idNodo) != ConstantesObservador.NO_EVIDENCIA) {
                    evidencias.add(evidencia.get(idNodo));
                }
            }
        }
        if (nombreNodosSolicitados.size() != solicitudesCorrectas) { //nodo solicitado no existe
            for (final Nodo nodoActual : this.nodos) {
                if (nodosAEliminarOrdenados.contains(nodoActual.getNombre())) {
                    nodosAEliminarOrdenados.remove(nodoActual.getNombre());
                }
            }
            String mensajeExcepcion = "No se localizan nodos con nombres";
            for (final String nombNodo : nodosAEliminarOrdenados) {
                mensajeExcepcion = mensajeExcepcion + " " + nombNodo;
            }
            mensajeExcepcion = mensajeExcepcion + " al calcular la probabilidad.";
            throw new ExcepcionNodoInexistente(mensajeExcepcion);
        }
        if (evidencias.size() == solicitudesCorrectas) {
            final List<Nodo> nodosFactorNuevo = new ArrayList<Nodo>();
            final List<Double> evidenciasFactores = new ArrayList<Double>();
            evidenciasFactores.add(1.0);
            return new Factor(nodosFactorNuevo, evidenciasFactores);
        }

        final List<String> ordenNombreNodos = new ArrayList<String>();
        for (final int idNodo : ordenIdNodos) {
            ordenNombreNodos.add(nodos.get(idNodo).getNombre());
        }
        final List<Factor> factores = new ArrayList<Factor>();
        double constante = 1;
        Factor factorActual;
        List<String> evidenciasNodos;
        List<Integer> evidenciasEstados;
        for (final Nodo nodo : nodos) {
            if (nodo.tieneEstadoFijado()) {
                nodosAEliminarOrdenados.remove(nodo.getNombre());
            }
            factorActual = nodo.factor().ordena(ordenNombreNodos);
            evidenciasNodos = new ArrayList<String>();
            evidenciasEstados = new ArrayList<Integer>();
            for (int idNodo = 0; idNodo < numNodos; idNodo++) {
                if (this.evidencia.get(idNodo) != ConstantesObservador.NO_EVIDENCIA) {
                    nombreNodo = getNodo(idNodo).getNombre();
                    if (factorActual.tieneVariable(nombreNodo)) {
                        evidenciasNodos.add(nombreNodo);
                        evidenciasEstados.add(this.evidencia.get(idNodo));
                    }
                }
            }
            if (!evidenciasNodos.isEmpty()) {
                //LOG vv
                Log.append("Reducción por evidencias ");
                for (final String nombNodo : evidenciasNodos) {
                    Log.append("[" + nombNodo + "=" + getNodo(nombNodo).estadoFijadoToNombre() + "] ");
                }
                Log.append(" del factor:\n");
                cargaFactorEnLog(factorActual);
                Log.append("\n");
                //LOG ^^
                factorActual = factorActual.evidencia(evidenciasNodos, evidenciasEstados);
                //LOG vv
                Log.append("Factor reducido:\n");
                cargaFactorEnLog(factorActual);
                Log.append("\n");
                //LOG ^^
            }
            if (factorActual.getNodos().isEmpty()) {
                constante *= factorActual.getFactor(0);
            } else {
                factores.add(factorActual);
            }
        }
        final Factor factorNoNormalizado = sumaProductoVE(factores, nodosAEliminarOrdenados, ordenNombreNodos);
        final Factor alpha = factorNoNormalizado.sumatorio(nombreNodosSolicitados);
        if ((!alpha.getNodos().isEmpty())) {
            final List<Nodo> alphaNodos = alpha.getNodos();
            String mensajeNodos = "Al intentar normalizar el factor resultado no normalizado [";
            for (final Nodo nodoAux : factorNoNormalizado.getNodos()) {
                mensajeNodos = mensajeNodos + nodoAux.getNombre() + ", ";
            }
            mensajeNodos = mensajeNodos + "] sumando sobre [";
            for (final String nodoNombreAux : nombreNodosSolicitados) {
                mensajeNodos = mensajeNodos + nodoNombreAux + ", ";
            }
            mensajeNodos = mensajeNodos + "], se encontró un factor con nodos:";
            for (final Nodo nodoActual : alphaNodos) {
                mensajeNodos = mensajeNodos + " " + nodoActual.getNombre();
            }
            mensajeNodos = mensajeNodos + " en vez de un double.";
            throw new ExcepcionFactor(mensajeNodos);
        }
        final BigDecimal alphaBD = BigDecimal.valueOf(alpha.getFactor(0));
        Log.append("Constantes = " + constante + "\n");// LOG
        Log.append("alpha (denominador para normalizar) = " + alpha.getFactor(0) + "\n\n");// LOG
        return factorNoNormalizado.normalizar(alphaBD.doubleValue());
    }

    /**
     * Realiza la eliminación de variables.
     *
     * @param factores Lista de factores iniciales.
     * @param nodosAEliminar Nombre de los nodos a eliminar.
     * @return factor resultante de la eliminación de variables.
     * @throws ExcepcionNodoInexistente Si no existe un nodo solicitado.
     */
    private Factor sumaProductoVE(List<Factor> factores, final List<String> nodosAEliminar, final List<String> ordenNombreNodos) throws ExcepcionNodoInexistente {
        for (String variableAEliminar : nodosAEliminar) {
            factores = sumaProductoEliminaVariable(factores, variableAEliminar, ordenNombreNodos);
        }
        return productoFactores(factores, ordenNombreNodos);
    }

    /**
     * Realiza la eliminación de una variable en el algoritmo de eliminación.
     *
     * @param factores Lista de factores del estado actual.
     * @param variableAEliminar Nombre del nodo a eliminar en el paso.
     * @return lista de factores existentes tras completar el paso actual.
     * @throws ExcepcionNodoInexistente Si no existe un nodo solicitado.
     */
    private List<Factor> sumaProductoEliminaVariable(final List<Factor> factores, final String variableAEliminar, final List<String> ordenNombreNodos) throws ExcepcionNodoInexistente {
        Log.append("Eliminar variable " + variableAEliminar + "\n");
        List<Factor> factoresConVariable = new ArrayList<Factor>();
        List<Factor> factoresSinVariable = new ArrayList<Factor>();
        for (final Factor factorActual : factores) {
            if (factorActual.tieneVariable(variableAEliminar)) {
                factoresConVariable.add(factorActual);
            } else {
                factoresSinVariable.add(factorActual);
            }
        }
        //LOG vv
        if (factoresConVariable.size() > 1) {
            Log.append("Se multplican los factores:\n\n");
            for (Factor factorMultiplicar : factoresConVariable) {
                cargaFactorEnLog(factorMultiplicar);
            }
        }
        //LOG ^^
        Factor tau = productoFactores(factoresConVariable, ordenNombreNodos); //Factor intermedio
        //LOG vv
        Log.append("Se realiza sumatorio sobre " + variableAEliminar + " del factor:\n");
        cargaFactorEnLog(tau);
        Log.append("\n");
        //LOG ^^
        tau = tau.sumatorio(variableAEliminar); //Factor final
        //LOG vv
        Log.append("tau(");
        String nodosTauNombreLog = "";
        for (Nodo nodo : tau.getNodos()) {
            nodosTauNombreLog += nodo.getNombre() + ", ";
        }
        final int tamNodosTauNombreLog = nodosTauNombreLog.length();
        if (tamNodosTauNombreLog > 0) {
            nodosTauNombreLog = nodosTauNombreLog.substring(0, (tamNodosTauNombreLog - 2));
        }
        Log.append(nodosTauNombreLog + "){" + variableAEliminar + "}:\n");
        cargaFactorEnLog(tau);
        //LOG ^^
        factoresSinVariable.add(tau);
        return factoresSinVariable;
    }

    /**
     * Realiza el producto de varios factores.
     *
     * @param factores Lista de factores a multiplicar
     * @return factor resultante de la operación.
     * @throws ExcepcionNodoInexistente Si no existe un nodo solicitado.
     */
    private Factor productoFactores(final List<Factor> factores, final List<String> ordenNombreNodos) throws ExcepcionNodoInexistente {
        Factor resultado = factores.get(0);
        factores.remove(0);
        for (final Factor factorActual : factores) {
            resultado = resultado.multiplica(factorActual, ordenNombreNodos);
        }
        return resultado;
    }

    private void cargaFactorEnLog(final Factor factor) {
        if (factor.getNodos().size() == 0) {
            Log.append(factor.getFactor(0) + "\n\n");
        } else {
            final List<Integer> estadoActual = new ArrayList<Integer>();
            final List<Integer> maxElementos = new ArrayList<Integer>();
            for (Nodo nodo : factor.getNodos()) {
                Log.append("[ " + String.format("%1$15s", nodo.getNombre()) + " ] ");
                estadoActual.add(0);
                maxElementos.add(nodo.getNumEstados());
            }
            Log.append("\n");
            do {
                for (final int i : estadoActual) {
                    Log.append("  " + String.format("%1$15s", i) + "   ");
                }
                Log.append("  :   " + factor.getFactor(estadoActual) + "\n");
            } while (FuncionesComunes.siguienteCombinacion(estadoActual, maxElementos));
            Log.append("\n");
        }
    }
}