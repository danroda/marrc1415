/*
 * Desarrollo de una pequeña herramienta que permita representar y razonar
 * (realizando inferencias) sobre una Red Bayesiana de una forma sencilla.
 * Métodos avanzados de razonamiento y representación del conocimiento, Máster
 * en Ingeniería Informática, curso 2014-2015, Universidad de Valladolid.
 * Autores: Borja Cerezo, Cristian Tejedor y Daniel Roda.
 */
package proyecto1415MARRC.cliente;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import proyecto1415MARRC.logger.Log;
import proyecto1415MARRC.redBayesiana.ExcepcionNodoInexistente;
import proyecto1415MARRC.redBayesiana.Factor;
import proyecto1415MARRC.redBayesiana.FuncionesComunes;
import proyecto1415MARRC.redBayesiana.IRedBayesiana;
import proyecto1415MARRC.redBayesiana.Nodo;
import proyecto1415MARRC.redBayesiana.RedBayesiana;

/**
 * Cliente (Fase 2) monousuario y monotarea que prueba la aplicación en modo
 * texto las siguientes funciones:
 *
 * 1.Lectura y carga de fichero de datos (.red2) con formato:
 *
 * Windows: ".\\docs\\Fase2\\FicherosRed\\CespedMojado.red2"
 *
 * Linux: "./docs/Fase2/FicherosRed/CespedMojado.red2"
 *
 * 2. Fijar evidencias.
 *
 * 3. Eliminar evidencias.
 *
 * 4. Cálculo de probabilidades mediante el algoritmo de Eliminación de
 * Variables.
 *
 * 5. Mostrar la red: matriz de adyacencia, DPCs, probabilidades marginales y la
 * conjunta.
 *
 * Además registra en un log las acciones realizadas en la sesión. Acumulando
 * sesiones en el mismo fichero. Dicho fichero se llama log_MARRC1415.txt y se
 * encuentra en el mismo directorio donde se ejecute la aplicación.
 *
 *
 * @author marrc1415
 * @since 0.1-ALPHA
 * @version 1.0-FINAL
 */
public final class ClienteTextual {

    /**
     * Codificación de los caracteres al leer por línea de comandos y en el
     * documento .red2
     *
     * En el caso que no funcione correctamente alternar con "UTF-8" u otros.
     */
    private static final String CODIFICACION = "iso-8859-1";
    private static final String SEPARADOR = ";";
    private static final String SEPARADOR_MEDIO = "*";
    private static final String SEPARADOR_FINAL = SEPARADOR + SEPARADOR_MEDIO + SEPARADOR;
    private static final String COMENTARIOS = "#";
    private static final String SEPARADOR_LIMITE = "__________________________";
    private static final String SEPARADOR_ELEMENTO = "\"";
    private static final String SEPARADOR_PROBABILIDAD = "\t";
    private static final String FLAG_NOMBRE = "Nombre:";
    private static final String FLAG_ESTADOS = "Estados:";
    private static final String FLAG_EVIDENCIA = "Estado fijado:";
    private static final String FLAG_PADRES = "Padres:";
    private static final String FLAG_TABLADPC = "Tabla de probabilidad:";
    private static final int TAM_ELEMENTOS = 18;

    /**
     * Método que ejecuta el cliente. Muestra las opciones a realizar en la
     * aplicación.
     *
     * @param args Argumentos por línea de comandos.
     */
    public static void main(final String[] args) {
        System.out.println("++++++++++++++++++++++\nBienvenido a MARRC1415\n++++++++++++++++++++++\nAplicación creada por:\n   - Borja Cerezo\n   - Cristian Tejedor\n   - Daniel Roda\n++++++++++++++++++++++\n\n");
        Log.append(new StringBuilder("+++++++++++++++++++\n").append(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date())).append("\n+++++++++++++++++++\n").toString()); // Dejar una línea de separación para la siguiente interacción del log.
        Log.volcado();
        menu();
    }

    /**
     * Muestra un menú de opciones al usuario. No se abandona dicho menú hasta
     * que lo indique el usuario.
     */
    private static void menu() {
        IRedBayesiana red = null;
        final short OPCION_SALIR = 0;
        final short OPCION_CARGA_FICHERO = 1;
        final short OPCION_FIJAR_EVIDENCIA = 2;
        final short OPCION_ELIMINAR_EVIDENCIA = 3;
        final short OPCION_PROBABILIDAD = 4;
        final short OPCION_VER_RED = 5;
        final short OPCION_LOG = 6;
        final short OPCION_NADA = -1;
        String entradaTeclado;
        int opcion;
        Log.append("1 - Cargar fichero\n");
        while (red == null) {
            try {
                System.out.print("Primero indique la ruta del fichero .red2 a cargar\n -> ");
                red = cargaFichero(new BufferedReader(new InputStreamReader(System.in, CODIFICACION)).readLine(), red);
            } catch (final Exception ex) {
                System.out.println(" !!!< " + ex.getMessage() + " >\n");
            }
        }
        String opcionLog = "";
        final StringBuilder menuMensaje = new StringBuilder("¿Qué quiere hacer?\n").append(OPCION_SALIR).append(" - Salir\n")
                .append(OPCION_CARGA_FICHERO).append(" - Cargar fichero\n")
                .append(OPCION_FIJAR_EVIDENCIA).append(" - Fijar evidencia\n")
                .append(OPCION_ELIMINAR_EVIDENCIA).append(" - Eliminar evidencia\n")
                .append(OPCION_PROBABILIDAD).append(" - Calcular probabilidad\n")
                .append(OPCION_VER_RED).append(" - Ver red\n");
        do {
            opcionLog = Log.estaActivo() ? " - Desactivar log\n" : " - Activar log\n";
            final StringBuilder menuMensajeIteracion = new StringBuilder (menuMensaje)
                    .append(OPCION_LOG).append(opcionLog)
                    .append(OPCION_SALIR).append(" - Salir\n");
            System.out.println(menuMensajeIteracion);
            Log.volcado();
            try {
                entradaTeclado = new BufferedReader(new InputStreamReader(System.in, CODIFICACION)).readLine();
                opcion = Integer.parseInt(entradaTeclado);
                switch (opcion) {
                    case OPCION_CARGA_FICHERO:
                        System.out.print("Indique la ruta del fichero .red2 a cargar\n -> ");
                        Log.append("1 - Cargar fichero\n");
                        red = cargaFichero(new BufferedReader(new InputStreamReader(System.in, CODIFICACION)).readLine(), red);
                        break;
                    case OPCION_FIJAR_EVIDENCIA:
                        System.out.println("Fijo evidencia\n");
                        Log.append("2 - Fijo evidencia\n");
                        fijarEvidencia(red);
                        break;
                    case OPCION_ELIMINAR_EVIDENCIA:
                        Log.append("3 - Elimino evidencia\n");
                        System.out.println("Elimino evidencia\n");
                        eliminarEvidencia(red);
                        break;
                    case OPCION_PROBABILIDAD:
                        Log.append("4 - Calcular probabilidad\n");
                        System.out.println("Calcular probabilidad\n");
                        probabilidad(red);
                        break;
                    case OPCION_VER_RED:
                        Log.append("5 - Ver red\n");
                        System.out.println("Ver red\n");
                        printGrafo(red);
                        break;
                    case OPCION_LOG:
                        if (Log.estaActivo()){
                            Log.desactivar();
                            System.out.println("\nLog desactivado\n\n");
                        } else {
                            Log.activar();
                            System.out.println("\nLog activado\n\n");
                        }
                        break;
                }
            } catch (final Exception ex) {
                // No se hace nada, se le permite al usuario de nuevo elegir opción.
                System.out.println("No se reconoce la acción indicada.\n");
                opcion = OPCION_NADA;
            }
        } while (opcion != OPCION_SALIR);
        Log.append("\n"); // Dejar una línea de separación para la siguiente interacción del log.
        Log.volcado();
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\nSe ha guardado el log de la sesión en ./log_MARRC1415.txt\nGracias por utilizar MARRC1415\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n");
    }

    /**
     * Carga un fichero especificado.
     *
     * @param ruta Uri del fichero a cargar.
     */
    private static IRedBayesiana cargaFichero(String ruta, final IRedBayesiana redOriginal) {
        final String MSG_ERROR_CARGA = "\nHubo un error al cargar el fichero";
        try {
            final String redExtension1 = ".red";
            final String redExtension2 = ".red2";
            if (ruta.endsWith(redExtension1)) {
                return cargaFichero1(ruta, redOriginal);
            } else if (ruta.endsWith(redExtension2)) {
                return cargaFichero2(ruta, redOriginal);
            } else {
                throw new Exception("La extesión del fichero debe ser " + redExtension1 + " o " + redExtension2);
            }
        } catch (final Exception e) {
            System.out.println(MSG_ERROR_CARGA + ":\n !!!< " + e.getMessage() + " >\n");
        }
        return redOriginal;
    }

    /**
     * Carga un fichero especificado en el modo antiguo (.red).
     *
     * @param ruta Uri del fichero a cargar.
     */
    private static IRedBayesiana cargaFichero1(final String ruta, final IRedBayesiana redOriginal) {
        IRedBayesiana red = new RedBayesiana();
        final String MSG_ERROR_CARGA = "\nHubo un error al cargar el fichero";
        File archivo;
        FileReader fr = null;
        BufferedReader br;

        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            final String extension = ruta.substring(ruta.lastIndexOf('.') + 1, ruta.length());
            final String redExtension = "red";
            if (extension.compareToIgnoreCase(redExtension) != 0) {
                throw new Exception("La extesión del fichero debe ser ." + redExtension);
            }
            archivo = new File(ruta);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            String linea;
            // Fase 1 - Lectura de nodos
            Log.append("\n  Fase 1: ");
            boolean faseTerminada = false;
            while (!faseTerminada) {
                if (!(linea = br.readLine()).startsWith(COMENTARIOS)) {
                    faseTerminada = linea.endsWith(SEPARADOR_FINAL);
                    for (final String nombreNodo : linea.split(SEPARADOR)) {
                        if (nombreNodo.compareTo(SEPARADOR_MEDIO) != 0) {
                            Log.append(nombreNodo + ' ');
                            red.addNodo(nombreNodo);
                        }
                    }
                }
            }

            // Fase 2 - Lectura de Matriz de adyacencia
            Log.append("\n  Fase 2: ");
            faseTerminada = false;
            final ArrayList<ArrayList<Integer>> matrizAdyacencia = new ArrayList<ArrayList<Integer>>();
            for (int fila = 0; !faseTerminada;) {
                if (!(linea = br.readLine()).startsWith(COMENTARIOS)) {
                    faseTerminada = linea.endsWith(SEPARADOR_FINAL);
                    matrizAdyacencia.add(new ArrayList<Integer>());
                    for (final String enteroCadena : linea.split(SEPARADOR)) {
                        if (enteroCadena.compareTo(SEPARADOR_MEDIO) != 0) {
                            Log.append(enteroCadena + ' ');
                            matrizAdyacencia.get(fila).add(Integer.parseInt(enteroCadena));
                        }
                    }
                    fila++;
                }
            }
            red.setAdyacencia(matrizAdyacencia);

            // Fase 3 - Lectura de estados de nodos
            Log.append("\n  Fase 3: ");
            faseTerminada = false;
            for (int numNodo = 0; !faseTerminada;) {
                if (!(linea = br.readLine()).startsWith(COMENTARIOS)) {
                    faseTerminada = linea.endsWith(SEPARADOR_FINAL);
                    for (final String nombreEstado : linea.split(SEPARADOR)) {
                        if (nombreEstado.compareTo(SEPARADOR_MEDIO) != 0) {
                            Log.append(nombreEstado + ' ');
                            red.addEstadoNodo(numNodo, nombreEstado);
                        }
                    }
                    numNodo++;
                }
            }

            // Fase 4 - Lectura de Matrices DPC
            Log.append("\n  Fase 4: ");
            Boolean finDoc = false;
            ArrayList<ArrayList<Double>> dpc;
            for (int numNodo = 0; !finDoc;) {
                dpc = new ArrayList<ArrayList<Double>>();
                faseTerminada = false;
                for (int fila = 0; !faseTerminada && !finDoc;) {
                    if (finDoc = ((linea = br.readLine()) == null)) {
                    } else if (!linea.startsWith(COMENTARIOS)) {
                        faseTerminada = linea.endsWith(SEPARADOR_FINAL);
                        dpc.add(new ArrayList<Double>());
                        for (final String doubleCadena : linea.split(SEPARADOR)) {
                            if (doubleCadena.compareTo(SEPARADOR_MEDIO) != 0) {
                                Log.append(doubleCadena + ' ');
                                dpc.get(fila).add(Double.parseDouble(doubleCadena));
                            }
                        }
                        fila++;
                    }
                }
                if (dpc.size() != 0) {
                    red.setTablaDPCNodo(numNodo, dpc);
                }
                numNodo++;
            }
            System.out.print("\n--> Carga correcta.");
            Log.append("\n\nFichero cargado correctamente.\nCarga de Tablas DPC correctas (suman 1 en cada fila y tamaño apropiado para el nodo).\nComprobación correcta de Grafo acíclico.\n");
        } catch (final Exception e) {
            System.out.println(MSG_ERROR_CARGA + ":\n  !!!<" + e.getMessage() + " >\n");
            red = redOriginal;
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta
            // una excepcion.
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (final Exception e2) {
                System.out.println(MSG_ERROR_CARGA + '\n');
                e2.printStackTrace();
            }
        }
        System.out.println('\n');
        return red;
    }

    /**
     * Carga un fichero especificado en el nuevo modo (.red2).
     *
     * @param ruta Uri del fichero a cargar.
     */
    private static IRedBayesiana cargaFichero2(final String ruta, final IRedBayesiana redOriginal) {
        IRedBayesiana red = new RedBayesiana();
        final String MSG_ERROR_CARGA = "\nHubo un error al cargar el fichero";
        File archivo;
        FileReader fr = null;
        BufferedReader br;

        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            final String extension = ruta.substring(ruta.lastIndexOf('.') + 1, ruta.length());
            final String redExtension = "red2";
            if (extension.compareToIgnoreCase(redExtension) != 0) {
                throw new Exception("La extesión del fichero debe ser ." + redExtension);
            }
            archivo = new File(ruta);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            String linea;
            // Fase 1 - Lectura de nodos, estados y evidencias
            Log.append("\n  Fase 1:");
            while ((linea = br.readLine()) != null) {
                if (linea.startsWith(FLAG_NOMBRE)) {
                    String nombreNodo = leeElementos(linea).get(0);
                    if (nombreNodo != null) {
                        Log.append("\n   Generado " + nombreNodo);
                        red.addNodo(nombreNodo);
                        linea = br.readLine();
                        boolean encontrado = false;
                        while ((!encontrado) && (linea != null) && !(linea.startsWith(SEPARADOR_LIMITE))) {
                            if (linea.startsWith(FLAG_ESTADOS)) {
                                for (String nombreEstado : leeElementos(linea)) {
                                    Log.append("\n    Añadido estado " + nombreEstado + " de " + nombreNodo);
                                    red.addEstadoNodo(nombreNodo, nombreEstado);
                                    encontrado = true;
                                }
                            }
                            if (!encontrado) {
                                linea = br.readLine();
                            }
                        }
                        while ((linea != null) && !(linea.startsWith(SEPARADOR_LIMITE))) {
                            if (linea.startsWith(FLAG_EVIDENCIA)) {
                                String estadoFijado = leeElementos(linea).get(0);
                                if (estadoFijado != null) {
                                    Log.append("\n    Fijada evidencia " + estadoFijado + " de " + nombreNodo);
                                    red.fijaEstadoNodo(nombreNodo, estadoFijado);
                                }
                            }
                            linea = br.readLine();
                        }
                    }
                }
            }

            // Fase 2 - Lectura de relaciones y de tablas de distrinbución de probabilidad condicionada
            Log.append("\n\n  Fase 2:");
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            while ((linea = br.readLine()) != null) {
                if (linea.startsWith(FLAG_NOMBRE)) {
                    String nombreNodo = leeElementos(linea).get(0);
                    if (nombreNodo != null) {
                        Log.append("\n   Analizando " + nombreNodo);
                        linea = br.readLine();
                        boolean encontrado = false;
                        while ((!encontrado) && (linea != null) && !(linea.startsWith(SEPARADOR_LIMITE))) {
                            if (linea.startsWith(FLAG_PADRES)) {
                                for (String nombrePadre : leeElementos(linea)) {
                                    Log.append("\n    Añadido padre " + nombrePadre + " a " + nombreNodo);
                                    red.addRelacion(nombrePadre, nombreNodo);
                                }
                            } else if (linea.startsWith(FLAG_TABLADPC)) {
                                linea = br.readLine();
                                encontrado = true;
                                final ArrayList<ArrayList<Double>> tablaProbabilidadCondicionada = new ArrayList<ArrayList<Double>>();
                                while ((linea != null) && !(linea.startsWith(SEPARADOR_LIMITE))) {
                                    ArrayList<Double> fila = new ArrayList<Double>();
                                    for (final String probabilidad : linea.split(SEPARADOR_PROBABILIDAD)) {
                                        if (probabilidad.length() > 0) {
                                            fila.add(Double.parseDouble(probabilidad));
                                        }
                                    }
                                    tablaProbabilidadCondicionada.add(fila);
                                    linea = br.readLine();
                                }
                                Log.append("\n    Añadido tabla de probabilidad condicionada a " + nombreNodo);
                                red.setTablaDPCNodo(nombreNodo, tablaProbabilidadCondicionada);
                            }

                            if (!encontrado) {
                                linea = br.readLine();
                            }
                        }
                    }
                }
            }
            red.compruebaAciclico();
            System.out.println("\nCarga de fichero correcta.\n");
            Log.append("\n\nFichero cargado correctamente.\nCarga de Tablas DPC correctas (suman 1 en cada fila y tamaño apropiado para el nodo).\nComprobación correcta de Grafo acíclico.\n");
        } catch (final Exception e) {
            System.out.println(MSG_ERROR_CARGA + ":\n !!!< " + e.getMessage() + " >\n");
            red = redOriginal;
        } finally {
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta
            // una excepcion.
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (final Exception e2) {
                System.out.println(MSG_ERROR_CARGA + '\n');
                e2.printStackTrace();
            }
        }

        System.out.println('\n');
        return red;
    }

    /**
     * Lee una lista de elementos del formato cargaFichero2.
     */
    private static List<String> leeElementos(final String linea) {
        final List<String> elementos = new ArrayList<String>();
        int par = -1;
        for (final String elemento : linea.split(SEPARADOR_ELEMENTO)) {
            if (par == 1) {
                elementos.add(elemento);
            }
            par = par * -1;
        }
        return elementos;
    }

    /**
     * Permite fijar la evidencia de un nodo.
     *
     * @param red Red Bayesiana sobre la que se trabaja.
     */
    private static void fijarEvidencia(final IRedBayesiana red) {
        final String MSG_ERROR_FIJAR_EVIDENCIA = "\nHubo un error al fijar evidencia";
        try {
            System.out.print("Indique el nombre del nodo\n -> ");
            String nombreNodo = new BufferedReader(new InputStreamReader(System.in, CODIFICACION)).readLine();
            System.out.print("Indique el estado a fijar en el nodo\n -> ");
            String estadoNodo = new BufferedReader(new InputStreamReader(System.in, CODIFICACION)).readLine();
            red.fijaEstadoNodo(nombreNodo, estadoNodo);
            System.out.println("\nEvidencia Fijada\n");
            Log.append("\nEvidencia Fijada" + nombreNodo + "=" + estadoNodo + "\n");
        } catch (final Exception e) {
            System.out.println(MSG_ERROR_FIJAR_EVIDENCIA + ":\n !!!< " + e.getMessage() + " >\n");
        }
    }

    /**
     * Permite eliminar la evidencia de un nodo.
     *
     * @param red Red Bayesiana sobre la que se trabaja.
     */
    private static void eliminarEvidencia(IRedBayesiana red) {
        final String MSG_ERROR_ELIMINAR_EVIDENCIA = "\nHubo un error al eliminar evidencia";
        try {
            System.out.print("Indique el nombre del nodo (Deje vacío si desea eliminar todas las evidencias)\n -> ");
            final String nombreNodo = new BufferedReader(new InputStreamReader(System.in, CODIFICACION)).readLine();
            if (nombreNodo.length() == 0) {
                red.clearAllEstadosNodos();
                System.out.println("\nEvidencias eliminadas\n");
                Log.append("\nEvidencias eliminadas\n");
            } else {
                red.clearEstadoNodo(nombreNodo);
                System.out.println("\nEvidencia eliminada\n");
                Log.append("\nEvidencia del nodo '" + nombreNodo + "' eliminada\n");
            }
        } catch (final Exception e) {
            System.out.println(MSG_ERROR_ELIMINAR_EVIDENCIA + ":\n !!!< " + e.getMessage() + " >\n");
        }
    }

    /**
     * Muestra la probabilidad que se solicite de un nodo.
     *
     * @param red red cuya informacion se va a imprimir por pantalla.
     */
    private static void probabilidad(final IRedBayesiana red) {
        final String MSG_ERROR_PROBABILIDAD = "\nHubo un error al solicitar la probabilidad";
        try {
            System.out.print("Indique el nombre de un nodo cuya probabilidad desea ver (Deje vacío si desea ver la conjunta)\n -> ");
            String nombreNodo = new BufferedReader(new InputStreamReader(System.in, CODIFICACION)).readLine();
            if (nombreNodo.length() == 0) {
                printProbabilidadConjunta(red);
            } else {
                final List<String> variables = new ArrayList<String>();
                do {
                    variables.add(nombreNodo);
                    System.out.print("Indique el nombre de otro nodo cuya probabilidad desea ver (Deje vacío si no desea añadir más)\n -> ");
                    nombreNodo = new BufferedReader(new InputStreamReader(System.in, CODIFICACION)).readLine();
                } while (nombreNodo.length() != 0);
                printProbabilidades(variables, red);
            }
        } catch (final Exception e) {
            System.out.println(MSG_ERROR_PROBABILIDAD + ":\n !!!< " + e.getMessage() + " >\n");
        }
    }

    /**
     * Imprime por pantalla la informacion contenida en el grafo solicitado.
     *
     * @param red red cuya informacion se va a imprimir por pantalla.
     */
    private static void printGrafo(final IRedBayesiana red) {
        printAdyacencia(red);
        printAllDPC(red);
        printAllProbabilidades(red);
    }

    /**
     * Imprime por pantalla la informacion de la matriz de adyacencia contenida
     * en el grafo solicitado.
     *
     * @param red red cuya informacion se va a imprimir por pantalla.
     */
    private static void printAdyacencia(final IRedBayesiana red) {
        System.out.println("\n\nMatriz de adyacencia\n\n");
        final int numNodos = red.numNodos();
        for (int i = 0; i < numNodos; i++) {
            System.out.print("[" + ajustaCadena(i + "", 3) + "] ");
        }
        System.out.println();
        for (int idPadre = 0; idPadre < numNodos; idPadre++) {
            for (int idHijo = 0; idHijo < numNodos; idHijo++) {
                try {
                    if (red.hayRelacion(idPadre, idHijo)) {
                        System.out.print("  1   ");
                    } else {
                        System.out.print("  0   ");
                    }
                } catch (ExcepcionNodoInexistente ex) {
                    System.out.println(" !!!< " + ex.getMessage() + " >\n");
                }
            }
            try {
                System.out.println("[" + ajustaCadena(idPadre + "", 3) + "] " + red.getNodo(idPadre).getNombre());
            } catch (final ExcepcionNodoInexistente ex) {
                System.out.println(" !!!< " + ex.getMessage() + " >\n");
            }
        }
    }

    /**
     * Imprime por pantalla todas las tablas de probabilidad condicionada de los
     * nodos del grafo solicitado indexadas por id.
     *
     * @param red red cuya informacion se va a imprimir por pantalla.
     */
    private static void printAllDPC(final IRedBayesiana red) {
        final int numNodos = red.numNodos();
        for (int idNodo = 0; idNodo < numNodos; idNodo++) {
            printDPC(red, idNodo);
        }
    }

    /**
     * Imprime por pantalla la tabla de probabilidad condicionada del nodo
     * solicitado.
     *
     * @param red red cuya informacion se va a imprimir por pantalla.
     * @param idNodo id del nodo cuya tabla de probabilidad condicionada se va a
     * imprimir por pantalla.
     */
    private static void printDPC(final IRedBayesiana red, final int idNodo) {
        try {
            final Nodo nodo = red.getNodo(idNodo);
            final int numEstados = nodo.getNumEstados();
            final int numPadres = nodo.getNumPadres();
            System.out.println("Tabla DPC del nodo: " + nodo.getNombre());
            System.out.println(ajustaCadena(" ", (numPadres * (TAM_ELEMENTOS + 5))) +"( " +  ajustaCadena(nodo.getNombre(), ((TAM_ELEMENTOS +5) * numEstados -5)) + " ) ");
            for (final Nodo padre : nodo.getPadres()) {
                    System.out.print("( " + ajustaCadena(padre.getNombre(), TAM_ELEMENTOS) + "  )");
                }
            for (final String estado : nodo.getEstados()) {
                System.out.print("[ " + ajustaCadena(estado, TAM_ELEMENTOS) + " ] ");
            }
            System.out.println();
            if (numPadres == 0) {
                for (final String estado : nodo.getEstados()) {
                    System.out.print("  " + ajustaCadena(nodo.getDPC(estado) + "", TAM_ELEMENTOS) + "   ");
                }
            } else {
                final List<Integer> numEstadosPadres = new ArrayList<Integer>();
                final List<Integer> recorreEstadosPadres = new ArrayList<Integer>();
                for (final Nodo padre : nodo.getPadres()) {
                    numEstadosPadres.add(padre.getNumEstados());
                    recorreEstadosPadres.add(0);
                }
                do {
                    int indicePadre = 0;
                    for (final Nodo padre : nodo.getPadres()) {
                        System.out.print("[ " + ajustaCadena(padre.getEstados().get(recorreEstadosPadres.get(indicePadre)), TAM_ELEMENTOS) + "  ] ");
                        indicePadre++;
                    }
                    for (int indiceEstado = 0; indiceEstado < numEstados; indiceEstado++) {
                        System.out.print("  " + ajustaCadena(nodo.getDPC(indiceEstado, recorreEstadosPadres) + "", TAM_ELEMENTOS) + "   ");
                    }
                    System.out.println();
                } while (FuncionesComunes.siguienteCombinacion(recorreEstadosPadres, numEstadosPadres));
            }
            System.out.println('\n');
        } catch (final Exception ex) {
            System.out.println(" !!!< " + ex.getMessage() + " >\n");
            ex.printStackTrace();
        }
    }

    /**
     * Imprime por pantalla todas las probabilidades de los estados de cada nodo
     * del grafo solicitado indexado por id.
     *
     * @param red red cuya informacion se va a imprimir por pantalla.
     */
    private static void printAllProbabilidades(final IRedBayesiana red) {
        final int numNodos = red.numNodos();
        try {
            List<String> listaNombreNodo;
            for (int idNodo = 0; idNodo < numNodos; idNodo++) {
                listaNombreNodo = new ArrayList<String>();
                listaNombreNodo.add(red.getNodo(idNodo).getNombre());
                printProbabilidades(listaNombreNodo, red);
            }
        } catch (final Exception ex) {
            System.out.println(" !!!< " + ex.getMessage() + " >\n");
        }
    }

    /**
     * Imprime por pantalla la probabilidad conjunta del grafo del grafo
     * solicitado.
     *
     * @param red red cuya informacion se va a imprimir por pantalla.
     */
    private static void printProbabilidadConjunta(final IRedBayesiana red) {
        final int numNodos = red.numNodos();
        try {
            final List<String> listaNombreNodo = new ArrayList<String>();
            for (int idNodo = 0; idNodo < numNodos; idNodo++) {
                listaNombreNodo.add(red.getNodo(idNodo).getNombre());
            }
            printProbabilidades(listaNombreNodo, red);
        } catch (final Exception ex) {
            System.out.println(" !!!< " + ex.getMessage() + " >\n");
        }
    }

    /**
     * Imprime por pantalla las probabilidades de cada estado del nodo
     * solicitado.
     *
     * @param nodo Nodo cuya tabla de probabilidad condicionada se va a imprimir
     * por pantalla.
     */
    private static void printProbabilidades(final List<String> nombreNodos, final IRedBayesiana red) {
        try {
            final List<Nodo> nodosSolicitados = new ArrayList<Nodo>();
            for (final String nombreNodo : nombreNodos) {
                nodosSolicitados.add(red.getNodo(nombreNodo));
            }
            final int numNodos = red.numNodos();
            final StringBuilder probabilidadCadenaPrevia = new StringBuilder("P(");
            for (final String nombreNodo : nombreNodos) {
                probabilidadCadenaPrevia.append(nombreNodo).append(", ");
            }
            String probabilidadCadena = probabilidadCadenaPrevia.substring(0, (probabilidadCadenaPrevia.length() - 2));
            String evidencia = "";
            for (int i = 0; i < numNodos; i++) {
                if (red.getNodo(i).tieneEstadoFijado()) {
                    evidencia = evidencia + red.getNodo(i).getNombre() + "=" + red.getNodo(i).estadoFijadoToNombre() + ", ";
                }
            }
            if (evidencia.length() > 0) {
                evidencia = evidencia.substring(0, (evidencia.length() - 2));
                probabilidadCadena = probabilidadCadena + " | " + evidencia;
            }
            probabilidadCadena = probabilidadCadena + ")";
            System.out.println();
            System.out.println(probabilidadCadena);
            Log.append("Se inician los cálculos para calcular " + probabilidadCadena + "\n\n"); //LOG
            final Factor factor = red.probabilidad(nombreNodos);
            Log.append(probabilidadCadena + ":\n"); //LOG
            String cadenaAux;
            final List<Nodo> variables = factor.getNodos();
            final List<Integer> estadoActual = new ArrayList<Integer>();
            final List<Integer> numElementos = new ArrayList<Integer>();
            final List<Integer> posicionPropia = new ArrayList<Integer>();
            final List<Integer> nodosConEvidencias = new ArrayList<Integer>();
            for (Nodo nodoFactor : variables) {
                posicionPropia.add(nombreNodos.indexOf(nodoFactor.getNombre()));
            }
            int numNodosSolicitados = nodosSolicitados.size();
            for (int i = 0; i < numNodosSolicitados; i++) {
                estadoActual.add(0);
                numElementos.add(nodosSolicitados.get(i).getNumEstados());
                if (!posicionPropia.contains(i)) {
                    nodosConEvidencias.add(i);
                }
                cadenaAux = "[ " + ajustaCadena(nodosSolicitados.get(i).getNombre() , TAM_ELEMENTOS) + " ] ";
                System.out.print(cadenaAux);
                Log.append(cadenaAux);//LOG
            }
            System.out.println();
            Log.append("\n");//LOG
            boolean cero;
            int numEvidencias, elemento;
            do {
                for (int i = 0; i < numNodosSolicitados; i++) {
                    cadenaAux = "  " + ajustaCadena(nodosSolicitados.get(i).getEstados().get(estadoActual.get(i)), TAM_ELEMENTOS) + "   ";
                    System.out.print(cadenaAux);
                    Log.append(cadenaAux);//LOG
                }
                cero = false;
                numEvidencias = nodosConEvidencias.size();
                for (int i = 0; i < numEvidencias && !cero; i++) {
                    elemento = nodosConEvidencias.get(i);
                    cero = (estadoActual.get(elemento) != nodosSolicitados.get(elemento).estadoFijadoToId());
                }
                if (cero) {
                    System.out.println(" : 0.0");
                    Log.append(" : 0.0\n");//LOG
                } else {
                    if (posicionPropia.size() == 0) {
                        cadenaAux = " : " + factor.getFactor(0) + "\n";
                        System.out.print(cadenaAux);
                        Log.append(cadenaAux);//LOG
                    } else {
                        final List<Integer> estadosFactor = new ArrayList<Integer>();
                        for (final int posicion : posicionPropia) {
                            estadosFactor.add(estadoActual.get(posicion));
                        }
                        cadenaAux = " : " + factor.getFactor(estadosFactor) + "\n";
                        System.out.print(cadenaAux);
                        Log.append(cadenaAux);//LOG
                    }
                }
            } while (FuncionesComunes.siguienteCombinacion(estadoActual, numElementos));
        } catch (final Exception ex) {
            System.out.println(" !!!< " + ex.getMessage() + " >\n");
        } finally {
            System.out.println();
        }
    }

    /**
     * Ajusta una cadena a un tamaño dado.
     *
     * @param cadenaOriginal cadena a reducir.
     * @param tamDestino tamaño en el que contener la cadena.
     * @return La cadena ajustada al tamaño dado
     */
    private static String ajustaCadena (final String cadenaOriginal, final int tamDestino) {
        if (tamDestino < 1) {
            return "";
        }
        final int tamOriginal = cadenaOriginal.length();
        if (tamOriginal > tamDestino) {
            return String.format("%1$"+ tamDestino + "s", cadenaOriginal.substring(0, tamDestino - 1) + ".");
        }
            return String.format("%1$"+ tamDestino + "s", cadenaOriginal);
    }
}