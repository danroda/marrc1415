# MARRC1415 #

Desarrollo de una pequeña herramienta que permita representar y razonar (realizando inferencias) sobre una Red Bayesiana de una forma sencilla.

Métodos avanzados de razonamiento y representación del conocimiento, Máster en Ingeniería Informática, curso 2014-2015, Universidad de Valladolid.




**Autores**

Cerezo Redondo,	Borja.

Roda Suárez,		Daniel.

Tejedor García,		Cristian.




**Estructura de directorios**


**/docs**

Toda la documentación del proyecto y ejemplos de ficheros .red a utilizar.



**/src**

Código fuente de la aplicación



**/ejecutar**

Ejecutable de la aplicación: MARRC1415.jar


Para ejecutar por **consola**:

     cd <ruta_al_proyecto>/ejecutar/

     java -jar "MARRC1415.jar" 



**/javadoc**

Documentación de la aplicación.
     Abrir el fichero ~/javadoc/index.html
     


**JDK recomendado**

JDK 7, en su defecto 6 ú 8.




**Otros**

El fichero "/nbproject/project.properties" intentad no actualizarlo en los "commit/push".