# Ejemplo de red de tres nodos cíclica.
#Nombre de los nodos
L;T;A;*;
#Matriz de adyacencia
0;1;0;
0;0;1;
1;0;0;*;
#Nombre de los estados de cada nodo
a;b;c;
1;2;
Si;No;*;
#Probabilidad
0.2;0.3;0.5;
0.1;0.37;0.53;*;
0.3;0.7;
0.8;0.2;
0.7;0.3;*;
0.1;0.9;
0.7;0.3;*;